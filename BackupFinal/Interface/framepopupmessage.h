#ifndef FRAMEPOPUPMESSAGE_H
#define FRAMEPOPUPMESSAGE_H

#include <QFrame>
#include <string>

using namespace std;

namespace Ui {
class FramePopupMessage;
}

class FramePopupMessage : public QFrame
{
    Q_OBJECT

public:
    explicit FramePopupMessage(QWidget *parent = 0);
    ~FramePopupMessage();
    void mensagem(std::string title, std::string msg);

private slots:
    void on_pushButton_clicked();

private:
    Ui::FramePopupMessage *ui;
};

#endif // FRAMEPOPUPMESSAGE_H
