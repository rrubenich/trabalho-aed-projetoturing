#include "framecadastrarlivro.h"
#include "ui_framecadastrarlivro.h"
#include "Controle/telagerenciar.h"

QLineEdit *leCodigoLivro;
QLineEdit *leAnoLivro;
QLineEdit *leNumExemplarLivro;
QLineEdit *leTituloLivro;
QLineEdit *leAutorLivro;
QLineEdit *leEditoraLivro;
QComboBox *cbAreaLivro;
QLineEdit *leEdicaoLivro;

QLabel *lblTituloTelaLivro;
QPushButton *pbCadastrarLivro;
QPushButton *pbLimparCamposLivro;

FrameCadastrarLivro::FrameCadastrarLivro(QWidget *parent) :QFrame(parent),ui(new Ui::FrameCadastrarLivro){
    ui->setupUi(this);
    this->setGeometry(0,20,800,580);

    lblTituloTelaLivro = ui->lblTitulo;
    pbCadastrarLivro = ui->pbCadastrarLivro;
    pbLimparCamposLivro = ui->pbLimparCampos;

    leCodigoLivro = ui->leCodigoLivro;
    leAnoLivro = ui->leAnoLivro;
    leNumExemplarLivro = ui->leNumExemplarLivro;
    leTituloLivro = ui->leTituloLivro;
    leAutorLivro = ui->leAutorLivro;
    leEditoraLivro = ui->leEditoraLivro;
    cbAreaLivro = ui->cbAreaLivro;
    leEdicaoLivro = ui->leEdicaoLivro;


}

FrameCadastrarLivro::~FrameCadastrarLivro(){
    delete ui;
}

void FrameCadastrarLivro::populaComboBoxAreas(){
    vector<Area> listaAreas = Dados::areaHash.ListaTodos();

    int tam = listaAreas.size();
    string areas[11];
    for(int i = 0; i < tam; i++){
        Area a = listaAreas[i];
        areas[a.getCodigo()-1] = a.getNome();
    }

    for(int i = 0; i < tam; i++){
        cbAreaLivro->addItem(areas[i].c_str());
    }
}

void FrameCadastrarLivro::limparCamposLivro(int x){
    if(x == 0){
        leCodigoLivro->clear();
        leAnoLivro->clear();
        leNumExemplarLivro->clear();
        leTituloLivro->clear();
        leAutorLivro->clear();
        leEditoraLivro->clear();
        cbAreaLivro->setCurrentIndex(0);
        leEdicaoLivro->clear();
    }
    else{
        leAnoLivro->clear();
        leNumExemplarLivro->clear();
        leTituloLivro->clear();
        leAutorLivro->clear();
        leEditoraLivro->clear();
        cbAreaLivro->setCurrentIndex(0);
        leEdicaoLivro->clear();
    }
}

void FrameCadastrarLivro::trocaTituloTelaLivro(string txt){
    lblTituloTelaLivro->setText(QString::fromStdString(txt));
    pbCadastrarLivro->setText(QString::fromStdString(txt));

    if(txt.compare("Informações Livro") == 0){
        pbCadastrarLivro->hide();
        pbLimparCamposLivro->hide();
        leAnoLivro->setEnabled(false);
        leNumExemplarLivro->setEnabled(false);
        leTituloLivro->setEnabled(false);
        leAutorLivro->setEnabled(false);
        leEditoraLivro->setEnabled(false);
        cbAreaLivro->setEnabled(false);
        leEdicaoLivro->setEnabled(false);
    }
    else{
        pbCadastrarLivro->show();
        pbLimparCamposLivro->show();
        leAnoLivro->setEnabled(true);
        leNumExemplarLivro->setEnabled(true);
        leTituloLivro->setEnabled(true);
        leAutorLivro->setEnabled(true);
        leEditoraLivro->setEnabled(true);
        cbAreaLivro->setEnabled(true);
        leEdicaoLivro->setEnabled(true);
    }
}

void FrameCadastrarLivro::on_pbLimparCampos_clicked(){
    limparCamposLivro(1);
}

void FrameCadastrarLivro::on_pbCancelar_clicked(){
    limparCamposLivro(0);
    if(lblTituloTelaLivro->text().toStdString().compare("Cadastrar Livro") == 0){
        this->hide();
    }
    else{
        this->hide();
        TelaGerenciar::frameGerenciarLivro->show();
    }
}

void FrameCadastrarLivro::recebeDados(Livro l){
    limparCamposLivro(0);
    Conversoes c;

    leCodigoLivro->setText(c.converteIntParaString(l.getCodigo()).c_str());
    leAnoLivro->setText(c.converteIntParaString(l.getAno()).c_str());
    leNumExemplarLivro->setText(c.converteIntParaString(l.getNumeroExemplares()).c_str());
    leTituloLivro->setText(l.getTitulo().c_str());
    leAutorLivro->setText(l.getAutor().c_str());
    leEditoraLivro->setText(l.getEditora().c_str());
    cbAreaLivro->setCurrentIndex(l.getArea().getCodigo()-1);
    leEdicaoLivro->setText(c.converteIntParaString(l.getEdicao()).c_str());

}

Livro FrameCadastrarLivro::retiraDados(){
    Conversoes c;

    int id = c.converteStringParaInt(leCodigoLivro->text().toStdString());
    int ano = c.converteStringParaInt(leAnoLivro->text().toStdString());
    int numExemplares = c.converteStringParaInt(leNumExemplarLivro->text().toStdString());
    string titulo = leTituloLivro->text().toStdString();
    string autor = leAutorLivro->text().toStdString();
    string editora = leEditoraLivro->text().toStdString();
    int edicao = c.converteStringParaInt(leEdicaoLivro->text().toStdString());

    string area = cbAreaLivro->currentText().toStdString();
    Area a;
    a = Dados::areaHash.ConsultaArea(area.c_str());

    Livro l = Livro(id,titulo,autor,editora,ano,numExemplares,a,edicao);

    return l;
}

void FrameCadastrarLivro::on_pbCadastrarLivro_clicked(){

    if(!(leCodigoLivro->text().isEmpty() &&
         leAnoLivro->text().isEmpty() &&
         leNumExemplarLivro->text().isEmpty() &&
         leTituloLivro->text().isEmpty() &&
         leAutorLivro->text().isEmpty() &&
         leEditoraLivro->text().isEmpty() &&
         leEdicaoLivro->text().isEmpty())){

        Livro l = retiraDados();

        if(lblTituloTelaLivro->text().toStdString().compare("Cadastrar Livro") == 0){
            Dados::livroHash.Insere(l,l.getTitulo(),Dados::controleCadastroLivro);
            TelaGerenciar::frameGerenciarLivro->carregarTabelaLivro(Dados::livroHash.ListaTodos());
            this->hide();
            Dados::popupMessage->mensagem("Mensagem!","Inserido com sucesso!");
        }
        else if(lblTituloTelaLivro->text().toStdString().compare("Editar Livro") == 0){
            Dados::livroHash.Edita(l,l.getTitulo());
            TelaGerenciar::frameGerenciarLivro->carregarTabelaLivro(Dados::livroHash.ListaTodos());
            this->hide();
            TelaGerenciar::frameGerenciarLivro->show();
            Dados::popupMessage->mensagem("Mensagem!","Editado com sucesso!");
        }

    }
    else{
        Dados::popupMessage->mensagem("Erro!","Preencha todos os campos!");
    }
}
