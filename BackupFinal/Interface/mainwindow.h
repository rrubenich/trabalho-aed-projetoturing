#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

#include "Controle/dados.h"
#include "Controle/telas.h"
#include "Controle/conversoes.h"
#include "Controle/telagerenciar.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    void minimizaJanelas();

private slots:
    void on_actionGerenciar_Livros_triggered();
    void on_actionGerenciar_Cliente_triggered();
    void on_actionGerenciar_Emprestimos_triggered();
    void on_actionGerenciar_Funcion_rio_triggered();
    void on_actionAdicionar_Livro_triggered();
    void on_actionAdicionar_Cliente_triggered();
    void on_actionAdicionar_Funcion_rio_triggered();
    void on_actionFazer_Empr_stimo_triggered();

    void on_actionLogout_triggered();

    void on_actionExit_triggered();

    void on_actionSalvar_triggered();

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
