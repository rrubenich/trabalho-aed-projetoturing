#ifndef FRAMEGERENCIARUSUARIO_H
#define FRAMEGERENCIARUSUARIO_H

#include "Controle/dados.h"

namespace Ui {
class FrameGerenciarUsuario;
}

class FrameGerenciarUsuario : public QFrame
{
    Q_OBJECT

public:
    explicit FrameGerenciarUsuario(QWidget *parent);
    ~FrameGerenciarUsuario();
    void carregarTabelaUsuario(vector<Usuario> v);
    int pegaIDUsuarioLinhaSelecionada();
    string pegaNomeUsuarioLinhaSelecionada();
    void limparCamposGerenciarUsuario(int x);

private slots:
    void on_pbCancelar_clicked();

    void on_pbEditarCliente_clicked();

    void on_pbExcluirCliente_clicked();

    void on_pbMaisInfos_clicked();

    void on_pbBuscar_clicked();

private:
    Ui::FrameGerenciarUsuario *ui;
};

#endif // FRAMEGERENCIARUSUARIO_H
