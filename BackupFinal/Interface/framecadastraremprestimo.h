#ifndef FRAMECADASTRAREMPRESTIMO_H
#define FRAMECADASTRAREMPRESTIMO_H

#include <QFrame>
#include "Controle/dados.h"
#include "Controle/conversoes.h"

namespace Ui {
class FrameCadastrarEmprestimo;
}

class FrameCadastrarEmprestimo : public QFrame
{
    Q_OBJECT

public:
    explicit FrameCadastrarEmprestimo(QWidget *parent);
    ~FrameCadastrarEmprestimo();
    void trocaTituloTelaEmprestimo(string txt);
    void recebeDados(Emprestimo e);
    Emprestimo retiraDados();
    void limparCamposEmprestimo(int x);

private slots:
    void on_pbLimparCampos_clicked();
    void on_pbCancelar_clicked();

    void on_pbRealizarEmprestimo_clicked();

private:
    Ui::FrameCadastrarEmprestimo *ui;

};

#endif // FRAMECADASTRAREMPRESTIMO_H
