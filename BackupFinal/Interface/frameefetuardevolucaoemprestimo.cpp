#include "frameefetuardevolucaoemprestimo.h"
#include "ui_frameefetuardevolucaoemprestimo.h"
#include "Controle/telagerenciar.h"

QLineEdit *leClienteDevolucaoEmp;
QLineEdit *leDataAtualDevolucaoEmp;
QLineEdit *leDataDevolucaoEmp;
QLineEdit *leDataEmprestimoDev;
QLineEdit *leDiaAposDevolucaoEmp;
QLineEdit *leLivroDevolucaoEmp;
QLineEdit *leCodigoDevolucaoEmp;

FrameEfetuarDevolucaoEmprestimo::FrameEfetuarDevolucaoEmprestimo(QWidget *parent) :QFrame(parent),ui(new Ui::FrameEfetuarDevolucaoEmprestimo){
    ui->setupUi(this); 
    this->setGeometry(0,20,800,580);

    leClienteDevolucaoEmp = ui->leCliente;
    leDataAtualDevolucaoEmp = ui->leDataAtual;
    leDataDevolucaoEmp = ui->leDataDevolucao;
    leDataEmprestimoDev = ui->leDataEmprestimo;
    leDiaAposDevolucaoEmp = ui->leDiasAposDevolucao;
    leLivroDevolucaoEmp = ui->leLivro;
    leCodigoDevolucaoEmp = ui->leCodigoDevolucaoEmp;
}

FrameEfetuarDevolucaoEmprestimo::~FrameEfetuarDevolucaoEmprestimo(){
    delete ui;
}

void FrameEfetuarDevolucaoEmprestimo::limparCamposEfetuarDevolucaoEmprestimo(){
    leClienteDevolucaoEmp->clear();
    leDataAtualDevolucaoEmp->clear();
    leDataDevolucaoEmp->clear();
    leDataEmprestimoDev->clear();
    leDiaAposDevolucaoEmp->clear();
    leLivroDevolucaoEmp->clear();
    leCodigoDevolucaoEmp->clear();
}

int FrameEfetuarDevolucaoEmprestimo::bissexto (int ano) {
    return (ano % 4 == 0) && ((ano % 100 != 0) || (ano % 400 == 0));
}

int FrameEfetuarDevolucaoEmprestimo::calculaDiasAposDevolucao(Data devolucao, Data atual){
    int dias_mes[2][13] = {{0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31},
                           {0, 31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31}};

    int idias, fdias;

    int def_anos = 0;

    int i;
    int dbissexto;

    idias = devolucao.getDia();
    dbissexto = bissexto (devolucao.getAno());
    for (i = devolucao.getMes() - 1; i > 0; --i)
        idias += dias_mes[dbissexto][i];

    fdias = atual.getDia();
    dbissexto = bissexto (atual.getAno());
    for (i = atual.getMes() - 1; i > 0; --i)
        fdias += dias_mes[dbissexto][i];

    int aux = devolucao.getAno();
    while (aux < atual.getAno())
        def_anos += 365 + bissexto(aux++);

    return def_anos - idias + fdias;

    return 0;
}

void FrameEfetuarDevolucaoEmprestimo::recebeDados(Emprestimo e){
    limparCamposEfetuarDevolucaoEmprestimo();
    Conversoes c;

    leCodigoDevolucaoEmp->setText(c.converteIntParaString(e.getCodigo()).c_str());
    leClienteDevolucaoEmp->setText(e.getUsuario().getNome().c_str());

    string dtDevolucao = Data::montaStringDataDevolucao(e.getData(),5);
    leDataDevolucaoEmp->setText(dtDevolucao.c_str());

    string dtEmprestimo = Data::montaStringData(e.getData());
    leDataEmprestimoDev->setText(dtEmprestimo.c_str());

    Data atual = Data(Dados::pegaDiaAtual(),Dados::pegaMesAtual(),Dados::pegaAnoAtual());
    string dtAtual = Data::montaStringData(atual);
    leDataAtualDevolucaoEmp->setText(dtAtual.c_str());

    Data devolucao = Data::montaDataString(Data::montaStringDataDevolucao(e.getData(),5));
    int dias = calculaDiasAposDevolucao(devolucao,atual);
    leDiaAposDevolucaoEmp->setText(c.converteIntParaString(dias).c_str());

    leLivroDevolucaoEmp->setText(e.getLivro().getTitulo().c_str());
}

void FrameEfetuarDevolucaoEmprestimo::on_pbCancelar_clicked(){
    this->hide();
    limparCamposEfetuarDevolucaoEmprestimo();
    TelaGerenciar::frameGerenciarEmprestimo->show();
}

void FrameEfetuarDevolucaoEmprestimo::on_pbEfetuarDevolucao_clicked(){
    Conversoes c;
    Dados::emprestimoHash.Deleta(c.converteStringParaInt(leCodigoDevolucaoEmp->text().toStdString()),leLivroDevolucaoEmp->text().toStdString().c_str());

    Emprestimo e = Dados::emprestimoHash.Consulta(leLivroDevolucaoEmp->text().toStdString().c_str(),c.converteStringParaInt(leCodigoDevolucaoEmp->text().toStdString()));

    e.getLivro().setNumeroExemplares(e.getLivro().getNumeroExemplares() + 1);

    TelaGerenciar::frameGerenciarEmprestimo->carregarTabelaEmprestimo(Dados::emprestimoHash.ListaTodos());
    this->hide();

    Dados::popupMessage->mensagem("Mensagem!","Devolução efetuada com sucesso!");

    TelaGerenciar::frameGerenciarEmprestimo->show();
}
