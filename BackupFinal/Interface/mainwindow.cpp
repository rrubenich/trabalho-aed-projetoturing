#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "QLayout"

MainWindow::MainWindow(QWidget *parent) :QMainWindow(parent), ui(new Ui::MainWindow){
    ui->setupUi(this);

    TelaGerenciar::frameGerenciarLivro = new FrameGerenciarLivro(this);
    TelaGerenciar::frameGerenciarUsuario = new FrameGerenciarUsuario(this);
    TelaGerenciar::frameGerenciarEmprestimo = new FrameGerenciarEmprestimo(this);
    TelaGerenciar::frameGerenciarFuncionario = new FrameGerenciarFuncionario(this);

    Telas::frameCadastrarLivro = new FrameCadastrarLivro(this);
    Telas::frameCadastrarUsuario = new FrameCadastrarUsuario(this);
    Telas::frameCadastrarFuncionario = new FrameCadastrarFuncionario(this);
    Telas::frameCadastrarEmprestimo = new FrameCadastrarEmprestimo(this);
    Telas::frameEfetuarDevolucaoEmprestimo = new FrameEfetuarDevolucaoEmprestimo(this);

    Dados::popupMessage = new FramePopupMessage(this);
    Dados::popupMessage->setGeometry(214,170,371,240);

    minimizaJanelas();

    TelaGerenciar::frameEfetuarLogin = new FrameEfetuarLogin(this);

    this->setFixedSize(this->width(),this->height());
}

MainWindow::~MainWindow(){
    delete ui;
}

void MainWindow::minimizaJanelas(){
    TelaGerenciar::frameGerenciarLivro->hide();
    TelaGerenciar::frameGerenciarUsuario->hide();
    TelaGerenciar::frameGerenciarEmprestimo->hide();
    TelaGerenciar::frameGerenciarFuncionario->hide();

    Telas::frameCadastrarLivro->hide();
    Telas::frameCadastrarUsuario->hide();
    Telas::frameCadastrarFuncionario->hide();
    Telas::frameCadastrarEmprestimo->hide();
    Telas::frameEfetuarDevolucaoEmprestimo->hide();
}

void MainWindow::on_actionGerenciar_Livros_triggered(){
    minimizaJanelas();
    TelaGerenciar::frameGerenciarLivro->show();
}

void MainWindow::on_actionGerenciar_Cliente_triggered(){
    minimizaJanelas();
    TelaGerenciar::frameGerenciarUsuario->show();
}

void MainWindow::on_actionGerenciar_Emprestimos_triggered(){
    minimizaJanelas();
    TelaGerenciar::frameGerenciarEmprestimo->show();
}

void MainWindow::on_actionGerenciar_Funcion_rio_triggered(){
    minimizaJanelas();
    TelaGerenciar::frameGerenciarFuncionario->show();
}

void MainWindow::on_actionAdicionar_Livro_triggered(){
    minimizaJanelas();
    Livro l = Livro();

    vector<Livro> v = Dados::livroHash.ListaTodos();
    l.setCodigo(v.size()+1);

    Telas::frameCadastrarLivro->limparCamposLivro(0);
    Telas::frameCadastrarLivro->recebeDados(l);
    Telas::frameCadastrarLivro->limparCamposLivro(1);
    Telas::frameCadastrarLivro->trocaTituloTelaLivro("Cadastrar Livro");
    Telas::frameCadastrarLivro->show();
}

void MainWindow::on_actionAdicionar_Cliente_triggered(){
    minimizaJanelas();
    Usuario u = Usuario();

    vector<Usuario> v = Dados::usuarioHash.ListaTodos();

    u.setCodigo(v.size()+1);

    Telas::frameCadastrarUsuario->limparCamposUsuario(0);
    Telas::frameCadastrarUsuario->recebeDados(u);
    Telas::frameCadastrarUsuario->limparCamposUsuario(1);
    Telas::frameCadastrarUsuario->trocaTituloTelaUsuario("Cadastrar Cliente");
    Telas::frameCadastrarUsuario->show();
}

void MainWindow::on_actionAdicionar_Funcion_rio_triggered(){
    minimizaJanelas();
    Funcionario f = Funcionario();

    vector<Funcionario> v = Dados::funcionarioHash.ListaTodos();

    f.setCodigo(v.size()+1);
    Data d = Data(Dados::pegaDiaAtual(), Dados::pegaMesAtual(), Dados::pegaAnoAtual());
    f.setAdmissao(d);

    Telas::frameCadastrarFuncionario->limparCamposFuncionario(0);
    Telas::frameCadastrarFuncionario->recebeDados(f);
    Telas::frameCadastrarFuncionario->limparCamposFuncionario(1);
    Telas::frameCadastrarFuncionario->trocaTituloTelaFuncionario("Cadastrar Funcionário");
    Telas::frameCadastrarFuncionario->show();
}

void MainWindow::on_actionFazer_Empr_stimo_triggered(){
    minimizaJanelas();
    Emprestimo e = Emprestimo();

    vector<Emprestimo> v = Dados::emprestimoHash.ListaTodos();

    e.setCodigo(v.size()+1);
    e.setFuncionario(Dados::funcionarioLogado);

    Data d = Data(Dados::pegaDiaAtual(), Dados::pegaMesAtual(), Dados::pegaAnoAtual());

    e.setData(d);

    Telas::frameCadastrarEmprestimo->limparCamposEmprestimo(0);
    Telas::frameCadastrarEmprestimo->recebeDados(e);
    Telas::frameCadastrarEmprestimo->limparCamposEmprestimo(1);
    Telas::frameCadastrarEmprestimo->trocaTituloTelaEmprestimo("Realizar Empréstimo");
    Telas::frameCadastrarEmprestimo->show();
}

void MainWindow::on_actionLogout_triggered(){
    minimizaJanelas();
    Telas::frameCadastrarEmprestimo->limparCamposEmprestimo(0);
    Telas::frameCadastrarFuncionario->limparCamposFuncionario(0);
    Telas::frameCadastrarLivro->limparCamposLivro(0);
    Telas::frameCadastrarUsuario->limparCamposUsuario(0);
    Telas::frameEfetuarDevolucaoEmprestimo->limparCamposEfetuarDevolucaoEmprestimo();

    TelaGerenciar::frameGerenciarEmprestimo->limparCamposGerenciarEmprestimo(0);
    TelaGerenciar::frameGerenciarFuncionario->limparCamposGerenciarFuncionario(0);
    TelaGerenciar::frameGerenciarLivro->limparCamposGerenciarLivro(0);
    TelaGerenciar::frameGerenciarUsuario->limparCamposGerenciarUsuario(0);

    TelaGerenciar::frameEfetuarLogin->limparCamposEfetuarLogin();
    TelaGerenciar::frameEfetuarLogin->show();

    Dados d;
    d.salvarArquivos();
}

void MainWindow::on_actionExit_triggered(){
    Dados d;
    d.salvarArquivos();
}

void MainWindow::on_actionSalvar_triggered(){
    Dados d;
    d.salvarArquivos();
}
