#include "framegerenciarusuario.h"
#include "ui_framegerenciarusuario.h"
#include "Controle/telas.h"

QTableView *tvResultadosUsuario;
QStandardItemModel *modelUsuario;
QLineEdit *leCampoBuscaUsuario;

FrameGerenciarUsuario::FrameGerenciarUsuario(QWidget *parent) :QFrame(parent), ui(new Ui::FrameGerenciarUsuario){
    ui->setupUi(this);
    this->setGeometry(0,20,800,580);

    //Insere colunas tabela
    modelUsuario = new QStandardItemModel(0,4,this); //0 Rows and 4 Columns
    modelUsuario->setHorizontalHeaderItem(0, new QStandardItem(QString("ID")));
    modelUsuario->setHorizontalHeaderItem(1, new QStandardItem(QString("Nome")));
    modelUsuario->setHorizontalHeaderItem(2, new QStandardItem(QString("Telefone")));
    modelUsuario->setHorizontalHeaderItem(3, new QStandardItem(QString("E-Mail")));

    //Popula tabela
    tvResultadosUsuario = ui->tvResultados;
    tvResultadosUsuario->setModel(modelUsuario);

    //Muda larguras colunas
    tvResultadosUsuario->setColumnWidth(0, 50);
    tvResultadosUsuario->setColumnWidth(1, 300);
    tvResultadosUsuario->setColumnWidth(2, 115);
    tvResultadosUsuario->setColumnWidth(3, 250);

    leCampoBuscaUsuario = ui->leCampoBusca;
}

void FrameGerenciarUsuario::carregarTabelaUsuario(vector<Usuario> v){
    Conversoes c;
    int linhas = 0;

    modelUsuario->removeRows(0,modelUsuario->rowCount());

    int tam = v.size();
    for(int i = 0; i < tam; i++){
        Usuario u = v[i];


        QList<QStandardItem*> newRow;

        QStandardItem *Col1 = new QStandardItem(QString(c.converteIntParaString(u.getCodigo()).c_str()));
        newRow.append(Col1);

        QStandardItem *Col2 = new QStandardItem(QString(u.getNome().c_str()));
        newRow.append(Col2);

        QStandardItem *Col3 = new QStandardItem(QString(u.getTelefone().c_str()));
        newRow.append(Col3);

        QStandardItem *Col4 = new QStandardItem(QString(u.getEmail().c_str()));
        newRow.append(Col4);

        modelUsuario->insertRow(linhas,newRow);
        linhas++;

    }

}

int FrameGerenciarUsuario::pegaIDUsuarioLinhaSelecionada(){
    if(tvResultadosUsuario->selectionModel()->hasSelection()){
        Conversoes c;

        int row = tvResultadosUsuario->selectionModel()->currentIndex().row();
        int col = 0;
        int usuario = c.converteStringParaInt(modelUsuario->item(row,col)->text().toStdString());

        return usuario;
    }
    else{
        return -1;
    }
}

string FrameGerenciarUsuario::pegaNomeUsuarioLinhaSelecionada(){
    if(tvResultadosUsuario->selectionModel()->hasSelection()){
        Conversoes c;

        int row = tvResultadosUsuario->selectionModel()->currentIndex().row();
        int col = 1;
        string usuario = modelUsuario->item(row,col)->text().toStdString();

        return usuario;
    }
    else{
        return "";
    }
}

void FrameGerenciarUsuario::limparCamposGerenciarUsuario(int x){
    if(x == 0){
        modelUsuario->removeRows(0,modelUsuario->rowCount());
        leCampoBuscaUsuario->clear();
    }
    else{;
        leCampoBuscaUsuario->clear();
    }
}

FrameGerenciarUsuario::~FrameGerenciarUsuario(){
    delete ui;
}

void FrameGerenciarUsuario::on_pbCancelar_clicked(){
    this->hide();
}

void FrameGerenciarUsuario::on_pbEditarCliente_clicked(){
    int id = pegaIDUsuarioLinhaSelecionada();
    string cliente = pegaNomeUsuarioLinhaSelecionada();

    if(id != -1){
        Usuario u = Dados::usuarioHash.Consulta(cliente,id);
        Telas::frameCadastrarUsuario->trocaTituloTelaUsuario("Editar Cliente");
        Telas::frameCadastrarUsuario->recebeDados(u);

        this->hide();
        Telas::frameCadastrarUsuario->show();
    }
    else{
        Dados::popupMessage->mensagem("Erro!","Tabela não possui linha selecionada!");
    }
}

void FrameGerenciarUsuario::on_pbMaisInfos_clicked(){
    int id = pegaIDUsuarioLinhaSelecionada();
    string cliente = pegaNomeUsuarioLinhaSelecionada();

    if(id != -1){
        Usuario u = Dados::usuarioHash.Consulta(cliente,id);
        Telas::frameCadastrarUsuario->trocaTituloTelaUsuario("Informações Cliente");
        Telas::frameCadastrarUsuario->recebeDados(u);

        this->hide();
        Telas::frameCadastrarUsuario->show();
    }
    else{
        Dados::popupMessage->mensagem("Erro!","Tabela não possui linha selecionada!");
    }
}

void FrameGerenciarUsuario::on_pbExcluirCliente_clicked(){
    int id = pegaIDUsuarioLinhaSelecionada();
    string cliente = pegaNomeUsuarioLinhaSelecionada();

    if(id != -1){
        Usuario u = Dados::usuarioHash.Consulta(cliente,id);
        Dados::usuarioHash.Deleta(u.getCodigo(),u.getNome());
        carregarTabelaUsuario(Dados::usuarioHash.ListaTodos());
    }
    else{
        Dados::popupMessage->mensagem("Erro!","Tabela não possui linha selecionada!");
    }
}

void FrameGerenciarUsuario::on_pbBuscar_clicked(){
    string busca = leCampoBuscaUsuario->text().toStdString();

    if(!busca.empty()){
        vector<Usuario> v = Dados::usuarioHash.ConsultaUsuarioFuncionarioPorNome(busca,Dados::controleListaUsuariosNome);
        carregarTabelaUsuario(v);
    }else{
        vector<Usuario> v = Dados::usuarioHash.ListaTodos();
        carregarTabelaUsuario(v);
    }
}
