#-------------------------------------------------
#
# Project created by QtCreator 2014-06-23T16:08:28
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = TuringAED-Final
TEMPLATE = app


SOURCES += main.cpp \
    Interface/framecadastraremprestimo.cpp \
    Interface/framecadastrarfuncionario.cpp \
    Interface/framecadastrarlivro.cpp \
    Interface/framecadastrarusuario.cpp \
    Interface/frameefetuardevolucaoemprestimo.cpp \
    Interface/frameefetuarlogin.cpp \
    Interface/framegerenciaremprestimo.cpp \
    Interface/framegerenciarfuncionario.cpp \
    Interface/framegerenciarlivro.cpp \
    Interface/framegerenciarusuario.cpp \
    Interface/mainwindow.cpp \
    Controle/dados.cpp \
    Controle/telas.cpp \
    Controle/conversoes.cpp \
    Controle/telagerenciar.cpp \
    Persistencia/area.cpp \
    Persistencia/arquivoAcervo.cpp \
    Persistencia/arquivoControle.cpp \
    Persistencia/arquivoEmprestimo.cpp \
    Persistencia/arquivoFuncionario.cpp \
    Persistencia/arquivoLog.cpp \
    Persistencia/arquivoUsuario.cpp \
    Persistencia/controle.cpp \
    Persistencia/data.cpp \
    Persistencia/emprestimo.cpp \
    Persistencia/endereco.cpp \
    Persistencia/funcionario.cpp \
    Persistencia/livro.cpp \
    Persistencia/log.cpp \
    Persistencia/usuario.cpp \
    Interface/framepopupmessage.cpp

HEADERS  += \
    Interface/framecadastraremprestimo.h \
    Interface/framecadastrarfuncionario.h \
    Interface/framecadastrarlivro.h \
    Interface/framecadastrarusuario.h \
    Interface/frameefetuardevolucaoemprestimo.h \
    Interface/frameefetuarlogin.h \
    Interface/framegerenciaremprestimo.h \
    Interface/framegerenciarfuncionario.h \
    Interface/framegerenciarlivro.h \
    Interface/framegerenciarusuario.h \
    Interface/mainwindow.h \
    Controle/dados.h \
    Controle/telas.h \
    Controle/conversoes.h \
    Controle/telagerenciar.h \
    Persistencia/area.h \
    Persistencia/arquivoAcervo.h \
    Persistencia/arquivoControle.h \
    Persistencia/arquivoEmprestimo.h \
    Persistencia/arquivoFuncionario.h \
    Persistencia/arquivoLog.h \
    Persistencia/arquivoUsuario.h \
    Persistencia/controle.h \
    Persistencia/data.h \
    Persistencia/emprestimo.h \
    Persistencia/endereco.h \
    Persistencia/funcionario.h \
    Persistencia/hashAberto.h \
    Persistencia/hashFechado.h \
    Persistencia/lista.h \
    Persistencia/livro.h \
    Persistencia/log.h \
    Persistencia/usuario.h \
    Interface/framepopupmessage.h

FORMS    += \
    Interface/framecadastraremprestimo.ui \
    Interface/framecadastrarfuncionario.ui \
    Interface/framecadastrarlivro.ui \
    Interface/framecadastrarusuario.ui \
    Interface/frameefetuardevolucaoemprestimo.ui \
    Interface/frameefetuarlogin.ui \
    Interface/framegerenciaremprestimo.ui \
    Interface/framegerenciarfuncionario.ui \
    Interface/framegerenciarlivro.ui \
    Interface/framegerenciarusuario.ui \
    Interface/mainwindow.ui \
    Interface/framepopupmessage.ui

RESOURCES += \
    Interface/icons.qrc

OTHER_FILES +=
