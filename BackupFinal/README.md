# README #

## Trabalho de Algoritmos e Estruturas de Dados ##

* Trabalho 1 - AED
* Versão 0.0.1

**Professor:** 
*André Gustavo Maletzke*

**Integrantes:** 
*Rafael Rubenich ,Ingo Guilherme Both Eyng ,Igor Francisco Bertuol*

### Objetivo: ###
Ampliar o exercício de gerenciamento de livros adicionando novas funcionalidades e otimizando as estruturas de dados utilizadas.

### Descrição: ###
Considere um sistema de cadastro de livros que permita realizar as seguintes operações:

1. Cadastrar livros, mantendo informações como título, autor(es), editora, ano, número de exemplares, área, edição – Arquivo I;* 
1. Cadastrar usuários que poderão realizar empréstimos dos livros, mantendo para isso informações como nome, RG, CPF, endereço, e-mail e telefone – Arquivo * II;
1. Cadastrar funcionário, mantendo informações como nome, data de admissão, CTPS, login e senha – Arquivo III;
1. Cadastrar empréstimo, relacionando informações do livro, usuário, funcionário que realizou o empréstimo. Serão permitidos empréstimos por no máximo 5 dias;
1. Consultar livros existentes na biblioteca de acordo com os seguintes critérios: área, editora, autor e título;
1. Consultar empréstimos realizados por data, funcionários e usuário. Cada consulta deverá apresentar informações referentes ao empréstimo, tais como área do livro, título, autor, data do empréstimo, usuário e funcionário;
1. Consultar usuário por nome;
1. Devolver um livro realizando as alterações necessárias em cada estrutura utilizada;
1. Listar todos os usuários;
1. Listar todos os livros por área.

Considerando que as informações sobre o acervo atual da biblioteca serão passadas por arquivo deverá ser realizada leitura do arquivo e a posterior escrita em arquivo, pois após a execução do software novos livros poderão ser cadastrados. As demais informações do sistema também serão carregadas a partir de arquivos e armazenadas posteriormente nos mesmos. Logo, os seguintes arquivos serão necessários:

* Arquivo de acervo;
* Arquivo com usuários;
* Arquivo com funcionários;
* Arquivo com empréstimos;
* Arquivo de log que deverá registrar todo o histórico de empréstimos realizados.

As seguintes ações do sistema deverão ser avaliadas em função do número de comparações e atribuições. Por exemplo, o cadastro de um novo livro envolve quantas atribuições e quantas comparações até que seja acomodado na estrutura de dados. O mesmo deve ser determinado para cada funcionalidade, descartando questões que envolvam interface e detalhes não pertinentes à funcionalidade deverão ser desconsiderados. A determinação desses valores deve ser realizada de maneira automática. Ao término da utilização do sistema deverá ser gerado um arquivo CSV com as seguintes informações:


```
#!csv

,Numero de Execucoes,Total de Comparacoes,Total de Atribuicoes
Leitura Arquivo Acervo,,, **FALTA**
Leitura Arquivo Funcionarios,,, **FALTA**
Leitura Arquivo Emprestimo,,, **FALTA**
Cadastro de Livro,,, **OK**
Cadastro de Usuario,,, **OK**
Cadastro de Emprestimo,,, **OK**
Consulta de Livro - Area,,, **OK**
Consulta de Livro - Titulo,,, **OK**
Consulta de Livro - Editora,,, **OK**
Consulta de Livro - Autor,,, **OK**
Consulta de Emprestimos - Data,,, **OK**
Consulta de Emprestimos - Funcionario,,, **OK**
Consulta de Emprestimos - Usuario,,, **OK**
Lista de Usuarios por Nome,,, **FALTA (porque vai ficar mais facil implementar com a interface)**
Lista de Livros por Area,,, **FALTA (porque vai ficar mais facil implementar com a interface)**

```
Arquivo CSV com a Performance do Sistema.

### Observações: ###
* O número e nome das áreas é limitado e conhecido (ver Arquivo I);
* Um empréstimo só poderá ser realizado se existir no mínimo dois exemplares no acervo;
* O trabalho deverá ser realizado em grupo (máximo 3 alunos);
* Cada grupo deverá criar um nome fictício e enviar um e-mail ao professor indicando o nome fictício e os integrantes até 20/05/2014 às 23:59;
* Os grupos devem manter sigilo sobre suas soluções;
* Os grupos serão ranqueados de acordo com a perfomance do sistema e aplicação adequada de conceitos estudados na disciplina até o momento;
* Primeira entrega: 25/06/2014;
* Após a primeira entrega será divulgado o ranking dos trabalhos;
* Após a divulgação do ranking cada grupo terá um prazo de dois dias pra realizar melhorias e enviar nova versão para compor o ranking final - Opcional;
* Falta de desonestidade, se detectada, implicará em nota zero;
* Os integrantes dos cinco grupos melhores ranqueados terão acréscimo de nota, a ser definido pelo professor após avaliação de todos os trabalhos. O acréscimo será maior para o primeiro e irá diminuir gradativamente para os 4 restantes;
* As estruturas de dados que poderão ser utilizadas são: lista, fila, pilha, hash, listas generalizadas e cruzadas;
* O sistema deverá ser desenvolvido na linguagem C;
* Também serão consideradas boas práticas de programação;
* Um README deverá ser entregue juntamente com a implementação explicando passo a passo como realizar a execução, caso esteja incompleto ou não seja possível executar em virtude o README estar inconsistente descontos na nota serão realizados;
* Por fim, cada grupo deverá elaborar uma apresentação de no máximo 10 minutos explicando sua solução com foco na estrutura de dados utilizada e nos algoritmos. Essa apresentação deverá ser entregue 24hs após a segunda entrega opcional ou em até 48hs após a primeira entrega;
* Qualquer atraso será descontado 20% da nota por dia.