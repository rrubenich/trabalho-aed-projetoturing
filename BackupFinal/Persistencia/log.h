#ifndef LOG_H
#define LOG_H

#include "livro.h"
#include "usuario.h"
#include "funcionario.h"

class Log{
public:
    Log();

    Log(Usuario usuario, Livro livro, Funcionario funcionario, Data data, Data registro);

    void setUsuario(Usuario usuario);
    void setLivro(Livro livro);
    void setFuncionario(Funcionario funcionario);
    void setData(Data data);
    void setRegistro(Data registro);

    Data getData();
    Data getRegistro();
    Usuario getUsuario();
    Livro getLivro();
    Funcionario getFuncionario();

private:
    Usuario usuario;
    Livro livro;
    Funcionario funcionario;
    Data data;
    Data registro;
};


#endif // LOG_H
