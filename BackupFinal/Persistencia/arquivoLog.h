#ifndef ARQUIVOLOG_H
#define ARQUIVOLOG_H

#include <Persistencia/lista.h>
#include <Persistencia/emprestimo.h>
#include <Persistencia/log.h>

class ArquivoLog
{
public:
    ArquivoLog();
    void gravaArquivo(Log logs);
    string getHeader();
private:
    const string header = "Usuario,Livro,Login Funcionario,Data de entrega,Registro";
};

#endif // ARQUIVOLOG_H
