/*
 * Classe Tabela Hash para hashing perfeito
 * A tabela HASH nunca pode ser menor que o número de inserções
 * pois caso seja, retorna erro (retirado o controle de erro
 * para diminuir o número de comparações)
 * (Usado para área pois o tamanho das áreas é fixo e conhecido)
 *
 */

#ifndef HASH_H
#define HASH_H

#include <string>
#include <vector>
#include <Persistencia/controle.h>

using namespace std;

template <class T>
class Hash{

private:
    T **tabela;
    int tamanho;

public:
    Hash(){
        //Caso seja instanciado sem o tamanho definido por algum motivo
        this->tamanho = 11;
        tabela = new T*[tamanho];
        for(int i = 0; i < tamanho; i++){
            tabela[i] = NULL;
        }
    }

    Hash(int tamanho){
        //Define e cria tabela de tamanho dinâmico (já alocando espaço na memória)
        this->tamanho = tamanho;
        tabela = new T*[tamanho];
        for(int i = 0; i < tamanho; i++){
            tabela[i] = NULL;
        }
    }


    ~Hash(){
        for(int i = 0; i < tamanho; i++){
            if (tabela[i] != NULL){
                delete tabela[i];
            }
        }

        delete[] tabela;
    }

    int funcaoHash2(string chave){
        int hash, posicao;

        hash = (int)chave[0];
        posicao = hash % tamanho;

        return posicao;
    }

    T ConsultaArea(string chave){
        int hash = funcaoHash2(chave);
        //Roda até achar o código
        while(chave.compare(tabela[hash]->getNome()) != 0){
            hash = (hash+1) % tamanho;
        }

        if(chave.compare(tabela[hash]->getNome()) == 0){
            return *tabela[hash];
        }else{
            return T();
        }
    }

    vector<T> ListaTodos(){
        vector<T> retorno;

        for(int i = 0; i < tamanho; i++){
            if (tabela[i] != NULL){
                retorno.push_back(*tabela[i]);
            }
        }
        return retorno;
    }

    void Insere(T item, string chave){
        int hash = funcaoHash2(chave);
        int i = 0;
        //Roda até achar um espaço NULL
        while(tabela[hash] != NULL){

            //Conflito
            hash = (hash+1) % tamanho;

        }

        //Delete pq ele criou como NULL
        delete tabela[hash];
        tabela[hash] = new T(item);
      }



};

#endif // HASH_H
