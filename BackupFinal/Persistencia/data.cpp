#include "data.h"

Data::Data(){

}

Data::Data(int dia, int mes, int ano){
    this->dia = dia;
    this->mes = mes;
    this->ano = ano;
}

void Data::setDia(int dia){
    this->dia = dia;
}

void Data::setMes(int mes){
    this->mes = mes;
}

void Data::setAno(int ano){
    this->ano = ano;
}

int Data::getDia(){
    return dia;
}

int Data::getMes(){
    return mes;
}

int Data::getAno(){
    return ano;
}

string Data::montaStringData(Data data){
    string dataString;
    Conversoes c;

    dataString = c.converteIntParaString(data.getDia()) + "/" +
           c.converteIntParaString(data.getMes()) + "/" +
           c.converteIntParaString(data.getAno());

    return dataString;
}

string Data::montaStringDataDevolucao(Data data, int diasParaDevolucao){
    int d = data.getDia();
    int m = data.getMes();
    int a = data.getAno();

    d = d + diasParaDevolucao;

    if(m == 1 || m == 3 || m == 5 || m == 7 || m == 8 || m == 10 || m == 12){
        if(d > 31){
            d = d - 31;
            m++;
            if(m > 12){
                m = m - 12;
                a++;
            }
        }
    }
    else if(m == 4 || m == 6 || m == 9 || m == 11){
        if(d > 30){
            d = d - 30;
            m++;
            if(m > 12){
                m = m - 12;
                a++;
            }
        }
    }
    else{
        if(a%4==0 && (a%100!=0 || a%400==0)){
            if(d > 29){
                d = d - 29;
                m++;
                if(m > 12){
                    m = m - 12;
                    a++;
                }
            }
        }
        else{
            if(d > 28){
                d = d - 28;
                m++;
                if(m > 12){
                    m = m - 12;
                    a++;
                }
            }
        }
    }

    string dataString;
    Conversoes c;

    dataString = c.converteIntParaString(d) + "/" +
           c.converteIntParaString(m) + "/" +
           c.converteIntParaString(a);

    return dataString;
}

Data Data::montaDataString(string data){
    char delimiter = '/';
    int start = 0;
    int tam = 0;
    Conversoes c;
    int v[3];

    for(int i = 0; i < data.size(); i++){
        if(data[i] == delimiter){
            v[tam] = c.converteStringParaInt(data.substr(start,i-start));
            start = i+1;
            tam++;
        }
    }

    v[tam] = c.converteStringParaInt(data.substr(start,data.size()-start));
    Data d = Data(v[0],v[1],v[2]);

    return d;
}
