#include "arquivoFuncionario.h"
#include <iostream>
#include <string>
#include <fstream>
#include <Persistencia/funcionario.h>

arquivoFuncionario::arquivoFuncionario(){

}

void arquivoFuncionario::setHeader(string header){
    this->header = header;
}

string arquivoFuncionario::getHeader(){
    return header;
}

void arquivoFuncionario::leArquivo(HashAberto<Funcionario> &funcionarios, Controle &controle){
    int atr = 0;
    int cmp = 0;
    int opr = 0;


    fstream arquivo;
    arquivo.open("Arquivos/Arquivo III - funcionario.csv");

    int i = 0, j= 1; atr = atr+2;
    string header;
    char ch;
    string coluna[5];

    if(arquivo.is_open()){
        getline(arquivo,header,(char)13);
        setHeader(header);
        atr = atr+2;

        cmp++;
        while(arquivo.get(ch)){
            cmp+=2;
            if(i == 4){
                getline(arquivo,coluna[i],(char)13);
                coluna[i] = ch+coluna[i];

                Data data = data.montaDataString(coluna[1]);
                Funcionario novo(j, coluna[0], data, coluna[2], coluna[3], coluna[4]);
                funcionarios.Insere(novo,coluna[3], controle);

                i = -1;
                j++;
                atr+=8;
                opr+3;
            }else if(ch == (char)34){
                getline(arquivo,coluna[i],(char)34);
                arquivo.get(ch);
                atr+=2;
            }else{
                getline(arquivo,coluna[i],(char)44);
                coluna[i] = ch + coluna[i];
                atr+=2;
            }
            i++;atr++;
        }
    }

    arquivo.close();

    controle.setAtribuicoes(atr);
    controle.setComparacoes(cmp);
    controle.setOperacoes(1);
}

void arquivoFuncionario::gravaArquivo(HashAberto<Funcionario> &funcionarios){
    fstream arquivo;
    vector<Funcionario> vetor = funcionarios.ListaTodos();

    Data data;

    string strData;

    arquivo.open("Arquivos/Arquivo III - funcionario.csv", ios::out);

    arquivo << getHeader();
    arquivo << (char)13;

    int tam = vetor.size();
    for(int i = 0; i < tam; i++){

        strData = data.montaStringData(vetor.at(i).getAdmissao());


        arquivo << vetor.at(i).getNome() << (char)44;
        arquivo << strData << (char)44;
        arquivo << vetor.at(i).getCTPS() << (char)44;
        arquivo << vetor.at(i).getLogin() << (char)44;
        arquivo << vetor.at(i).getSenha();
        arquivo << (char)13;
    }

    arquivo.close();

}
