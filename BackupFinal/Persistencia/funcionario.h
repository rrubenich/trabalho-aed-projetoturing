#ifndef FUNCIONARIO_H
#define FUNCIONARIO_H

#include <string>
#include <Persistencia/usuario.h>
#include <Persistencia/data.h>

using namespace std;

class Funcionario{

public:
    Funcionario();
    Funcionario(int codigo, string nome, Data admissao, string CTPS, string login, string senha);

    void setCodigo(int codigo);
    void setNome(string nome);
    void setAdmissao(Data admissao);
    void setCTPS(string CTPS);
    void setLogin(string login);
    void setSenha(string senha);

    int getCodigo();
    string getNome();
    Data getAdmissao();
    string getCTPS();
    string getLogin();
    string getSenha();

private:
    int codigo;
    string nome;
    Data admissao;
    string CTPS;
    string login;
    string senha;
};

#endif // FUNCIONARIO_H
