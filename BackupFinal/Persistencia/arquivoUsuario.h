#ifndef ARQUIVOUSUARIO_H
#define ARQUIVOUSUARIO_H

#include <Persistencia/usuario.h>
#include <Persistencia/hashAberto.h>


class arquivoUsuario{
public:
    arquivoUsuario();
    void leArquivo(HashAberto<Usuario> &usuarios, Controle &controle);
    void gravaArquivo(HashAberto<Usuario> &usuarios);
    void setHeader(string header);
    string getHeader();

private:
    string header;
};

#endif // ARQUIVOUSUARIO_H
