#ifndef CONTROLE_H
#define CONTROLE_H
#include <iostream>
#include <string>

using namespace std;

class Controle{

public:
    Controle();
    Controle(string funcao, int operacoes, int comparacoes, int atribuicoes);

    void setFuncao(string funcao);
    void setOperacoes(int operacoes);
    void setComparacoes(int comparacoes);
    void setAtribuicoes(int atribuicoes);

    string getFuncao();
    unsigned long int getOperacoes();
    unsigned long int getComparacoes();
    unsigned long int getAtribuicoes();

private:
    string funcao;
    int operacoes;
    int comparacoes;
    int atribuicoes;
};

#endif // CONTROLE_H
