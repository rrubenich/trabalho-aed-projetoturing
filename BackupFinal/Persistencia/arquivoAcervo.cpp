#include "arquivoAcervo.h"
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <fstream>
#include <Persistencia/livro.h>
#include <Controle/dados.h>

using namespace std;

arquivoAcervo::arquivoAcervo(){

}

int arquivoAcervo::strToInt(string str){
    return atoi(str.c_str());
}

void arquivoAcervo::setHeader(string header){
    this->header = header;
}

string arquivoAcervo::getHeader(){
    return header;
}

void arquivoAcervo::leArquivo(HashAberto<Livro> &livros, HashAberto<Area> &areas, Controle &controle){
    int atr = 0;
    int cmp = 0;
    int opr = 0;

    fstream arquivo;
    arquivo.open("Arquivos/Arquivo I - acervo.csv");

    string coluna[7];
    int i = 0; atr++;
    int j = 1; atr++;

    char ch;
    string header;

    cmp++;
    if(arquivo.is_open()){

        getline(arquivo,header,(char)13);
        setHeader(header);
        atr=+2;

        cmp++;
        while(arquivo.get(ch)){
            atr++;
            cmp+=2;
            if(i == 6){
                getline(arquivo,coluna[i],(char)13);
                coluna[i] = ch + coluna[i];
                atr+=2;

                Area area;
                area = areas.ConsultaArea(coluna[6].c_str(),controle);
                atr+=1;

                Livro novo(j,coluna[0],coluna[1],coluna[2],strToInt(coluna[4]),strToInt(coluna[5]),area,strToInt(coluna[3]));
                livros.Insere(novo,coluna[0],controle);

                i = -1;
                j++;

                atr+9;
            }else if(ch == (char)34){
                getline(arquivo,coluna[i],(char)34);
                arquivo.get(ch);

                atr+=2;

            }else{
                getline(arquivo,coluna[i],(char)44);
                coluna[i] = ch + coluna[i];

                atr+=2;
            }
            i++; atr++;
        }
    }

    arquivo.close();

    controle.setAtribuicoes(atr);
    controle.setComparacoes(cmp);
    controle.setOperacoes(1);
}

void arquivoAcervo::gravaArquivo(HashAberto<Livro> &livros){
    fstream arquivo;
    vector<Livro> vetor = livros.ListaTodos();

    arquivo.open("Arquivos/Arquivo I - acervo.csv", ios::out);
    arquivo << getHeader();

    int tam = vetor.size();
    for(int i = 0; i < tam; i++){
        arquivo << (char)13;
        arquivo << (char)34 << vetor.at(i).getTitulo() << (char)34 << (char)44;
        arquivo << (char)34 << vetor.at(i).getAutor() << (char)34 << (char)44;
        arquivo << vetor.at(i).getEditora() << (char)44;
        arquivo << vetor.at(i).getEdicao() << (char)44;
        arquivo << vetor.at(i).getAno() << (char)44;
        arquivo << vetor.at(i).getNumeroExemplares() << (char)44;
        arquivo << vetor.at(i).getArea().getNome();
    }

    arquivo.close();

}

