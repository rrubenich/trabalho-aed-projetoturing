#ifndef EMPRESTIMO_H
#define EMPRESTIMO_H

#include "livro.h"
#include "usuario.h"
#include "funcionario.h"

class Emprestimo{

public:
    Emprestimo();
    Emprestimo(int codigo, Usuario usuario, Livro livro, Funcionario funcionario, Data data);

    void setUsuario(Usuario usuario);
    void setLivro(Livro livro);
    void setFuncionario(Funcionario funcionario);
    void setCodigo(int codigo);
    void setData(Data data);
    Data getData();

    Usuario getUsuario();
    Livro getLivro();
    Funcionario getFuncionario();
    int getCodigo();

private:
    int codigo;
    Usuario usuario;
    Livro livro;
    Funcionario funcionario;
    Data data;
};

#endif // EMPRESTIMO_H
