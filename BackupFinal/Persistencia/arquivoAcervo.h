#ifndef ACERVO_H
#define ACERVO_H

#include <Persistencia/hashAberto.h>
#include <Persistencia/hashFechado.h>
#include <Persistencia/livro.h>
#include <Persistencia/controle.h>

class arquivoAcervo{
public:
    arquivoAcervo();
    int strToInt(string str);
    void leArquivo(HashAberto<Livro> &livros, HashAberto<Area> &areas, Controle &controle);
    void gravaArquivo(HashAberto<Livro> &livros);
    void setHeader(string header);
    string getHeader();

private:
    string header;
};

#endif // ACERVO_H
