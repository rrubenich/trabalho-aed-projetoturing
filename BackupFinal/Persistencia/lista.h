/*
 * Biblioteca com todas operacoes com lista encadeada do sistema
 *
 */

#ifndef LISTA_H
#define LISTA_H

#include <iostream>
#include <vector>
#include <Persistencia/controle.h>

using namespace std;

template <class T>
class Lista{
private:

    typedef struct celula{
        T item;
        celula* next;
    }* tipoCelula;

    tipoCelula head; //Cabeça da lista
    tipoCelula aux;  //Auxiliar

public:
    Lista(){
        head = NULL;
        aux = NULL;
    }

    void Insere(T novo){

        //Cria o objeto de nó
        tipoCelula n = new celula;
        n->next = NULL;
        n->item = novo;

        //Roda a lista para adiconar no ultimo

        if(head != NULL){
            aux = head;
            while(aux->next != NULL){
                aux = aux->next;
            }

            aux->next = n;
        }
        //Se não, adiciona na cabeça
        else{
            head = n;
        }
    }

    bool Deleta(int codigo){

        tipoCelula delItem = NULL;
        tipoCelula delAux = head;
        aux = head;

        //Se está na cabeça

        if(head->item.getCodigo() == codigo){
            delItem = head;
            head = head->next;
            delete delItem;

            return true;

        }else{
            //Roda a lista até encontrar ou até apontar pra null
            while(aux != NULL && aux->item.getCodigo() != codigo){
                delAux = aux;
                aux = aux->next;
            }

            if(aux == NULL){
                delete delItem;


                return false; //Não está na lista
            }
            else{
                delItem = aux;
                aux = aux->next;
                delAux->next = aux;
                delete delItem;

                return true;
            }
        }
    }

    vector<T> ListaTodos(){
        aux = head;

        vector<T> lista;

        while(aux != NULL){
            lista.push_back(aux->item);
            aux = aux->next;
        }

        return lista;
    }

    T Consulta(int codigo){
        aux = head;

        //Roda a lista até encontrar ou até apontar pra null
        while(aux != NULL && aux->item.getCodigo() != codigo){
            aux = aux->next;
        }

        if(aux == NULL){
            cout << codigo << " não está na lista\n";
            return T();
        }
        else{
            return aux->item;
        }
    }

    T ConsultaFuncionario(string chave){
        aux = head;

        //Roda a lista até encontrar ou até apontar pra null
        while(aux != NULL && aux->item.getLogin() != chave){
            aux = aux->next;
        }

        if(aux == NULL){
            return T();
        }
        else{
            return aux->item;
        }
    }

    void Atualiza(T item){
        aux = head;

        //Roda a lista até encontrar ou até apontar pra null
        while(aux != NULL && aux->item.getCodigo() != item.getCodigo()){
            aux = aux->next;
        }

        aux->item = item;

    }

    vector<T> Busca(string query, int identificador){
        aux = head;

        vector<T> retornoBusca;

        switch(identificador){

        //Busca livro por titulo
        case 1:
            while(aux != NULL){


                //Esse string.compare deveria funcionar, precisamos achar
                //alguma forma de comparar 2 strings tipo do SQL, pois
                //com o == " ab" é diferente de "ab"

                //if(aux->livro.getEditora().compare("query") != 0){
                //    retornoBusca.push_back(aux->livro);
                //}

                if(aux->livro.getTitulo() == query){
                    retornoBusca.push_back(aux->livro);
                }

                aux = aux->next;
            }
            break;
        //Autor
        case 2:
            while(aux != NULL){

                if(aux->livro.getAutor() == query){
                    retornoBusca.push_back(aux->livro);
                }

                aux = aux->next;
            }
            break;
        //Editora
        case 3:
            while(aux != NULL){

                if(aux->livro.getEditora() == query){
                    retornoBusca.push_back(aux->livro);
                }

                aux = aux->next;
            }
            break;
        default:
            break;
        }

        return retornoBusca;

    }

};

#endif // LISTA_H
