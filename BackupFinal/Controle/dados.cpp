#include "dados.h"
#include "telas.h"
#include "telagerenciar.h"

#include "Persistencia/arquivoAcervo.h"
#include "Persistencia/arquivoControle.h"
#include "Persistencia/arquivoEmprestimo.h"
#include "Persistencia/arquivoFuncionario.h"
#include "Persistencia/arquivoUsuario.h"

using namespace std;

time_t t = time(NULL);
struct tm *local = localtime(&t);

Controle Dados::controleLeituraAcervo;
Controle Dados::controleLeituraFuncionarios;
Controle Dados::controleLeituraEmprestimo;
Controle Dados::controleLeituraClientes;

Controle Dados::controleCadastroLivro;
Controle Dados::controleCadastroUsuario;
Controle Dados::controleCadastroEmprestimo;
Controle Dados::controleCadastroFuncionario;
Controle Dados::controleCadastroArea;

Controle Dados::controleConsultaLivroArea;
Controle Dados::controleConsultaLivroTitulo;
Controle Dados::controleConsultaLivroEditora;
Controle Dados::controleConsultaLivroAutor;

Controle Dados::controleConsultaEmprestimoData;
Controle Dados::controleConsultaEmprestimoFuncionario;
Controle Dados::controleConsultaEmprestimoUsuario;

Controle Dados::controleListaUsuariosNome;
Controle Dados::controleListaLivrosArea;

Controle Dados::controleDeletaFuncionario;

arquivoAcervo arqAcervo;
arquivoFuncionario arqFuncionario;
arquivoUsuario arqUsuario;
ArquivoEmprestimo arqEmprestimo;

Dados::Dados(){

}

void Dados::populaHash(){

    Dados::controleLeituraAcervo = Controle("Leitura Arquivo Acervo",0,0,0);
    Dados::controleLeituraFuncionarios = Controle("Leitura Arquivo Funcionarios",0,0,0);
    Dados::controleLeituraEmprestimo = Controle("Leitura Arquivo Emprestimo",0,0,0);
    Dados::controleLeituraClientes = Controle("Leitura Arquivo Cliente",0,0,0);

    Dados::controleCadastroLivro = Controle("Cadastro de Livro",0,0,0);
    Dados::controleCadastroUsuario = Controle("Cadastro de Usuario",0,0,0);
    Dados::controleCadastroEmprestimo = Controle("Cadastro de Emprestimo",0,0,0);
    Dados::controleCadastroFuncionario = Controle("Cadastro de Funcionario",0,0,0);
    Dados::controleCadastroArea = Controle("Cadastro de Area",0,0,0);

    Dados::controleConsultaLivroArea = Controle("Consulta de Livro - Area",0,0,0);
    Dados::controleConsultaLivroTitulo = Controle("Consulta de Livro - Titulo",0,0,0);
    Dados::controleConsultaLivroEditora = Controle("Consulta de Livro - Editora",0,0,0);
    Dados::controleConsultaLivroAutor = Controle("Consulta de Livro - Autor",0,0,0);

    Dados::controleConsultaEmprestimoData = Controle("Consulta de Emprestimos - Data",0,0,0);
    Dados::controleConsultaEmprestimoFuncionario = Controle("Consulta de Emprestimos - Funcionario",0,0,0);
    Dados::controleConsultaEmprestimoUsuario = Controle("Consulta de Emprestimos - Usuario",0,0,0);

    Dados::controleListaUsuariosNome = Controle("Lista de Usuarios por Nome",0,0,0);
    Dados::controleListaLivrosArea = Controle("Lista de Livros por Area",0,0,0);

    Dados::controleDeletaFuncionario = Controle("Deleta Funcionario",0,0,0);

    int i = 0;
    Area cc = Area(++i, "CC");
    Area mat = Area(++i, "MAT");
    Area em = Area(++i, "EM");
    Area ee = Area(++i, "EE");
    Area fls = Area(++i, "FLS");
    Area med = Area(++i, "MED");
    Area dir = Area(++i, "DIR");
    Area hist = Area(++i, "HIST");
    Area adm = Area(++i, "ADM");
    Area ec = Area(++i, "EC");
    Area qui = Area(++i, "QUI");

    Dados::areaHash.Insere(cc,cc.getNome(),Dados::controleCadastroArea);
    Dados::areaHash.Insere(mat,mat.getNome(),Dados::controleCadastroArea);
    Dados::areaHash.Insere(em,em.getNome(),Dados::controleCadastroArea);
    Dados::areaHash.Insere(ee,ee.getNome(),Dados::controleCadastroArea);
    Dados::areaHash.Insere(fls,fls.getNome(),Dados::controleCadastroArea);
    Dados::areaHash.Insere(med,med.getNome(),Dados::controleCadastroArea);
    Dados::areaHash.Insere(dir,dir.getNome(),Dados::controleCadastroArea);
    Dados::areaHash.Insere(hist,hist.getNome(),Dados::controleCadastroArea);
    Dados::areaHash.Insere(adm,adm.getNome(),Dados::controleCadastroArea);
    Dados::areaHash.Insere(ec,ec.getNome(),Dados::controleCadastroArea);
    Dados::areaHash.Insere(qui,qui.getNome(),Dados::controleCadastroArea);

    arqAcervo.leArquivo(Dados::livroHash,Dados::areaHash,Dados::controleLeituraAcervo);
    arqFuncionario.leArquivo(Dados::funcionarioHash,Dados::controleLeituraFuncionarios);
    arqUsuario.leArquivo(Dados::usuarioHash,Dados::controleLeituraClientes);
    arqEmprestimo.leArquivo(Dados::emprestimoHash,Dados::usuarioHash,Dados::funcionarioHash,Dados::livroHash,Dados::controleLeituraEmprestimo);

    Telas::frameCadastrarLivro->populaComboBoxAreas();

    TelaGerenciar::frameGerenciarLivro->populaComboBoxAreas();
}

void Dados::salvarArquivos(){
    arquivoControle arqControle;

    Lista<Controle> controles;

    controles.Insere(controleLeituraAcervo);
    controles.Insere(controleLeituraFuncionarios);
    controles.Insere(controleLeituraEmprestimo);
    controles.Insere(controleCadastroLivro);
    controles.Insere(controleCadastroUsuario);
    controles.Insere(controleCadastroEmprestimo);
    controles.Insere(controleConsultaLivroArea);
    controles.Insere(controleConsultaLivroTitulo);
    controles.Insere(controleConsultaLivroEditora);
    controles.Insere(controleConsultaLivroAutor);
    controles.Insere(controleConsultaEmprestimoData);
    controles.Insere(controleConsultaEmprestimoFuncionario);
    controles.Insere(controleConsultaEmprestimoUsuario);
    controles.Insere(controleListaUsuariosNome);
    controles.Insere(controleListaLivrosArea);

    arqControle.gravaArquivo(controles);

    arqAcervo.gravaArquivo(Dados::livroHash);
    arqFuncionario.gravaArquivo(Dados::funcionarioHash);
    arqUsuario.gravaArquivo(Dados::usuarioHash);
    arqEmprestimo.gravaArquivo(Dados::emprestimoHash);
}

int Dados::pegaDiaAtual(){
    return local->tm_mday;
}

int Dados::pegaMesAtual(){
    return local->tm_mon + 1;
}

int Dados::pegaAnoAtual(){
    return local->tm_year+1900;
}
