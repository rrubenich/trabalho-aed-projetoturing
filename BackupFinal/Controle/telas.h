#ifndef TELAS_H
#define TELAS_H

#include "Interface/framecadastraremprestimo.h"
#include "Interface/framecadastrarfuncionario.h"
#include "Interface/framecadastrarlivro.h"
#include "Interface/framecadastrarusuario.h"
#include "Interface/frameefetuardevolucaoemprestimo.h"

class Telas{
public:
    Telas();

    static FrameCadastrarLivro *frameCadastrarLivro;
    static FrameCadastrarUsuario *frameCadastrarUsuario;
    static FrameCadastrarFuncionario *frameCadastrarFuncionario;
    static FrameCadastrarEmprestimo *frameCadastrarEmprestimo;
    static FrameEfetuarDevolucaoEmprestimo *frameEfetuarDevolucaoEmprestimo;

};

#endif // TELAS_H
