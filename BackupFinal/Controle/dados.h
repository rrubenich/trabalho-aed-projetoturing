#ifndef DADOS_H
#define DADOS_H

#include <string>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <sstream>
#include <fstream>
#include <time.h>

#include <QFrame>
#include <QTableView>
#include <QStandardItem>
#include <QComboBox>

#include "Persistencia/area.h"
#include "Persistencia/controle.h"
#include "Persistencia/data.h"
#include "Persistencia/log.h"
#include "Persistencia/emprestimo.h"
#include "Persistencia/funcionario.h"
#include "Persistencia/hashAberto.h"
#include "Persistencia/hashFechado.h"
#include "Persistencia/lista.h"
#include "Persistencia/livro.h"
#include "Persistencia/arquivoLog.h"
#include "Persistencia/usuario.h"
#include "Persistencia/endereco.h"
#include "Interface/framepopupmessage.h"

using namespace std;

class Dados{
public:
    Dados();
    void populaHash();
    void salvarArquivos();

    static int pegaDiaAtual();
    static int pegaMesAtual();
    static int pegaAnoAtual();

    static Funcionario funcionarioLogado;

    static HashAberto<Livro> livroHash;
    static HashAberto<Usuario> usuarioHash;
    static HashAberto<Emprestimo> emprestimoHash;
    static HashAberto<Funcionario> funcionarioHash;
    static HashAberto<Area> areaHash;
    static ArquivoLog listaLogs;

    static FramePopupMessage *popupMessage;

    static Controle controleLeituraAcervo;
    static Controle controleLeituraFuncionarios;
    static Controle controleLeituraEmprestimo;
    static Controle controleLeituraClientes;

    static Controle controleCadastroLivro;
    static Controle controleCadastroUsuario;
    static Controle controleCadastroEmprestimo;
    static Controle controleCadastroFuncionario;
    static Controle controleCadastroArea;

    static Controle controleConsultaLivroArea;
    static Controle controleConsultaLivroTitulo;
    static Controle controleConsultaLivroEditora;
    static Controle controleConsultaLivroAutor;

    static Controle controleConsultaEmprestimoData;
    static Controle controleConsultaEmprestimoFuncionario;
    static Controle controleConsultaEmprestimoUsuario;

    static Controle controleListaUsuariosNome;
    static Controle controleListaLivrosArea;

    static Controle controleDeletaFuncionario;


};

#endif // DADOS_H
