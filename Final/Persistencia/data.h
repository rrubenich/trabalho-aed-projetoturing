#ifndef DATA_H
#define DATA_H

#include <vector>
#include <string>
#include <sstream>
#include <iostream>
#include <Controle/conversoes.h>

using namespace std;

class Data{
public:
    Data();
    Data(int dia, int mes, int ano);

    void setDia(int dia);
    void setMes(int mes);
    void setAno(int ano);
    int getDia();
    int getMes();
    int getAno();

    static string montaStringData(Data data);
    static string montaStringDataDevolucao(Data data, int diasParaDevolucao);
    static Data montaDataString(string data);

private:
    int dia;
    int mes;
    int ano;
};

#endif // DATA_H
