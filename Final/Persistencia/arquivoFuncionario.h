#ifndef ARQUIVOFUNCIONARIO_H
#define ARQUIVOFUNCIONARIO_H

#include <Persistencia/hashAberto.h>
#include <Persistencia/controle.h>
#include <Persistencia/funcionario.h>

class arquivoFuncionario{

public:
    arquivoFuncionario();
    void leArquivo(HashAberto<Funcionario> &funcionarios, Controle &controle);
    void gravaArquivo(HashAberto<Funcionario> &funcionarios);
    void setHeader(string header);
    string getHeader();

private:
    string header;
};

#endif // ARQUIVOFUNCIONARIO_H
