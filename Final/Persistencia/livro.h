#ifndef LIVRO_H
#define LIVRO_H

#include <string>
#include <Persistencia/area.h>

using namespace std;

class Livro{

public:
    Livro();
    Livro(int codigo, string titulo, string autor, string editora, int ano, int numeroExemplares, Area area, int edicao);

    void setCodigo(int codigo);
    void setTitulo(string titulo);
    void setAutor(string autor);
    void setEditora(string editora);
    void setAno(int ano);
    void setNumeroExemplares(int numeroExemplares);
    void setCodigoArea(int codigoArea);
    void setArea(Area area);
    void setEdicao(int edicao);

    int getCodigo();
    string getTitulo();
    string getAutor();
    string getEditora();
    int getAno();
    int getNumeroExemplares();
    int getCodigoArea();
    Area getArea();
    int getEdicao();

private:
    int codigo;
    string titulo;
    string autor;
    string editora;
    int ano;
    int numeroExemplares;
    Area area;
    int edicao;
};

#endif // LIVRO_H
