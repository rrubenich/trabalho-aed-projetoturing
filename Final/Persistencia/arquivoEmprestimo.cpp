#include "arquivoEmprestimo.h"
#include <iostream>
#include <string>
#include <fstream>
#include <Persistencia/livro.h>
#include <Persistencia/lista.h>
#include <Controle/conversoes.h>

ArquivoEmprestimo::ArquivoEmprestimo(){

}

void ArquivoEmprestimo::setHeader(string header){
    this->header = header;
}

string ArquivoEmprestimo::getHeader(){
    return header;
}

void ArquivoEmprestimo::leArquivo(HashAberto<Emprestimo> &emprestimos, HashAberto<Usuario> &usuarios, HashAberto<Funcionario> &funcionarios, HashAberto<Livro> &livros, Controle &controle){
    int atr = 0;
    int cmp = 0;
    int opr = 0;


    fstream arquivo;
    arquivo.open("Arquivos/Arquivo IV - emprestimo.csv");

    int i = 0, j= 1; atr+=2;
    string header;
    string coluna[6];
    char ch;

    if(arquivo.is_open()){
        getline(arquivo,header,(char)13);
        setHeader(header);
        atr+=2;

        cmp++;
        while(arquivo.get(ch)){cmp++;

            cmp++;
            if(i == 5){

                getline(arquivo,coluna[i],(char)13);
                coluna[i] = ch + coluna[i];

                Conversoes c;

                Data data = data.montaDataString(coluna[5]);
                Livro livro = livros.ConsultaLivroEdicao(coluna[0],c.converteStringParaInt(coluna[1]), controle);
                Usuario usuario = usuarios.ConsultaUsuario(coluna[2], controle);
                Funcionario funcionario = funcionarios.ConsultaFuncionario(coluna[3], controle);
                Emprestimo novo(j, usuario, livro, funcionario, data);
                emprestimos.Insere(novo,coluna[0],controle);

                atr+=13;
                opr+=6;
                i = -1;
                j++;
            }else if(ch == (char)34){
                getline(arquivo,coluna[i],(char)34);
                //coluna[i] = ch + coluna[i];
                arquivo.get(ch);

                atr+=3;

            }else{
                getline(arquivo,coluna[i],(char)44);
                coluna[i] = ch + coluna[i];

                atr+=3;
            }
            i++;atr++;
        }
    }

    controle.setAtribuicoes(atr);
    controle.setComparacoes(cmp);
    controle.setOperacoes(1);

    arquivo.close();
}


void ArquivoEmprestimo::gravaArquivo(HashAberto<Emprestimo> &emprestimos){
    fstream arquivo;
    vector<Emprestimo> vetor = emprestimos.ListaTodos();

    Data data;

    string strData;

    arquivo.open("Arquivos/Arquivo IV - emprestimo.csv");
    arquivo << getHeader();

    int tam = vetor.size();
    for(int i = 0; i < tam; i++){

        strData = data.montaStringData(vetor.at(i).getData());

        arquivo << (char)13;
        arquivo << (char) 34 << vetor.at(i).getLivro().getTitulo() << (char)34 << (char)44;
        arquivo << vetor.at(i).getLivro().getEdicao() << (char)44;
        arquivo << vetor.at(i).getUsuario().getNome() << (char)44;
        arquivo << vetor.at(i).getFuncionario().getLogin() << (char)44;
        arquivo << vetor.at(i).getFuncionario().getNome() << (char)44;
        arquivo << strData;
    }

    arquivo << (char)13;
    arquivo.close();

}

