#ifndef ARQUIVOEMPRESTIMO_H
#define ARQUIVOEMPRESTIMO_H

#include <Persistencia/hashAberto.h>
#include <Persistencia/hashFechado.h>
#include <Persistencia/emprestimo.h>
#include <Persistencia/controle.h>
#include <Persistencia/lista.h>

class ArquivoEmprestimo{
public:
    ArquivoEmprestimo();
    void leArquivo(HashAberto<Emprestimo> &emprestimos, HashAberto<Usuario> &usuarios, HashAberto<Funcionario> &funcionarios, HashAberto<Livro> &livros, Controle &controle);
    void gravaArquivo(HashAberto<Emprestimo> &emprestimos);
    void setHeader(string header);
    string getHeader();
private:
    string header;
};

#endif // EMPRESTIMO_H
