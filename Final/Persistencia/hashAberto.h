/*
 * Hashing Aberto utilizando uma string do item na função hash
 * (Usado para Usuario e Livro e Emprestimos)
 *
 * L = Livre
 * O = Ocupado
 */

#ifndef HASHABERTO_H
#define HASHABERTO_H

#include <string>
#include <iostream>
#include <vector>
#include "controle.h"
#include "data.h"

using namespace std;

template <class T>
class HashAberto{

private:

    static const int tamanhoTabela = 59;

    typedef struct celula{
        T item;
        celula* next;
        string marcador;
    }* tipoCelula;

    tipoCelula HashTable[tamanhoTabela];
    tipoCelula aux;


public:
    HashAberto(){

        //Número primo mais próximo de 55(teto) (Intervalo entre z-A)
        //tamanhoTabela = 59;
        for(int i = 0; i < tamanhoTabela; i++){
            HashTable[i] = new celula;
            HashTable[i]->marcador = "L";
            HashTable[i]->next = NULL;
        }
    }

    //Contabilizado nos outros métodos
    int funcaoHash(string chave){
        int hash, posicao;

        hash = (int)chave[0] + (int)chave[1] + (int)chave[2];

        posicao = hash % tamanhoTabela;

        return posicao;
    }

    void Insere(T item, string chave, Controle &controle){
        int atr = 0;
        int cmp = 0;
        int opr = 1;
        int posicao = funcaoHash(chave); atr+3; opr++;

        //Se a cabeça estiver vazia
        cmp++;
        if(HashTable[posicao]->marcador == "L"){
            HashTable[posicao]->item = item; atr++;
            HashTable[posicao]->marcador = ""; atr++;
        }
        else{
            aux = HashTable[posicao]; atr++;
            tipoCelula novo = new celula; atr++;
            novo->item = item; atr++;
            novo->next = NULL; atr++;

            cmp++;
            while(aux->next != NULL){ cmp++;
                aux = aux->next; atr++;
            }

            aux->next = novo; atr++;
        }

        controle.setAtribuicoes(atr);
        controle.setComparacoes(cmp);
        controle.setOperacoes(opr);
    }

    T Consulta(string chave, int codigo){
        int posicao = funcaoHash(chave);
        aux = HashTable[posicao];

        if(!(HashTable[posicao]->marcador == "L")){
            while(aux->item.getCodigo() != codigo || aux->next != NULL){
                aux = aux->next;
            }
            return aux->item;
        }
        else{
            return T();
        }
    }

    //Consulta pelo titulo do livro
    T ConsultaLivro(string chave){
        int posicao = funcaoHash(chave);
        aux = HashTable[posicao];

        if(!(HashTable[posicao]->marcador == "L")){
            while(chave.compare(aux->item.getTitulo()) != 0 || aux->next != NULL){
                aux = aux->next;
            }
            return aux->item;
        }
        else{
            return T();
        }
    }

    //Consulta pelo titulo do livro
    T ConsultaLivroEdicao(string chave, int edicao ,Controle &controle){
        int atr = 0;
        int cmp = 0;

        int posicao = funcaoHash(chave);
        aux = HashTable[posicao];

        atr+=4;
        cmp++;
        if(!(HashTable[posicao]->marcador == "L")){
            cmp+=2;
            while(chave.compare(aux->item.getTitulo()) != 0 || aux->item.getEdicao() != edicao || aux->next != NULL){
                cmp+=2;
                atr++;
                aux = aux->next;
            }
            return aux->item;
        }
        else{
            return T();
        }

        controle.setAtribuicoes(atr);
        controle.setComparacoes(cmp);
        controle.setOperacoes(2);
    }

    //Consulta pelo titulo da area com controle
    T ConsultaArea(string chave, Controle &controle){
        int atr = 0;
        int cmp = 0;

        int posicao = funcaoHash(chave);
        aux = HashTable[posicao];

        atr+=4;
        cmp++;
        if(!(HashTable[posicao]->marcador == "L")){
            cmp+=2;
            while(chave.compare(aux->item.getNome()) != 0 || aux->next != NULL){

                cmp+=2;
                atr++;
                aux = aux->next;
            }
            return aux->item;
        }
        else{
            return T();
        }

        controle.setAtribuicoes(atr);
        controle.setComparacoes(cmp);
        controle.setOperacoes(2);
    }

    //Consulta pelo nome do usurio
    T ConsultaUsuario(string chave, Controle &controle){
        int atr = 0;
        int cmp = 0;

        int posicao = funcaoHash(chave);
        aux = HashTable[posicao];

        atr+=4;
        cmp++;
        if(!(HashTable[posicao]->marcador == "L")){
            cmp+2;
            while(chave.compare(aux->item.getNome()) != 0 || aux->next != NULL){
                cmp+=2;
                atr++;
                aux = aux->next;
            }
            return aux->item;
        }
        else{
            return T();
        }

        controle.setAtribuicoes(atr);
        controle.setComparacoes(cmp);
        controle.setOperacoes(2);

    }

    //Consulta pelo titulo da area
    T ConsultaArea(string chave){
        int posicao = funcaoHash(chave);
        aux = HashTable[posicao];

        if(!(HashTable[posicao]->marcador == "L")){
            while(chave.compare(aux->item.getNome()) != 0 || aux->next != NULL){
                aux = aux->next;
            }
            return aux->item;
        }
        else{
            return T();
        }
    }

    //Consulta pelo nome do usurio
    T ConsultaUsuario(string chave){
        int posicao = funcaoHash(chave);
        aux = HashTable[posicao];

        if(!(HashTable[posicao]->marcador == "L")){
            while(chave.compare(aux->item.getNome()) != 0 || aux->next != NULL){
                aux = aux->next;
            }
            return aux->item;
        }
        else{
            return T();
        }
    }

    //Consulta pelo login do funcionario
    T ConsultaFuncionario(string chave, Controle &controle){
        int atr = 0;
        int cmp = 0;

        int posicao = funcaoHash(chave);
        aux = HashTable[posicao];

        atr+=4;
        cmp++;
        if(!(HashTable[posicao]->marcador == "L")){
            cmp+2;
            while(chave.compare(aux->item.getLogin()) != 0 || aux->next != NULL){
                cmp+=2;
                atr++;
                aux = aux->next;
            }
            return aux->item;
        }
        else{
            return T();
        }

        controle.setAtribuicoes(atr);
        controle.setComparacoes(cmp);
        controle.setOperacoes(2);

    }

    //Consulta pelo login do funcionario
    T ConsultaFuncionario(string chave){
        int posicao = funcaoHash(chave);
        aux = HashTable[posicao];

        if(!(HashTable[posicao]->marcador == "L")){
            while(chave.compare(aux->item.getLogin()) != 0 || aux->next != NULL){
                aux = aux->next;
            }
            return aux->item;
        }
        else{
            return T();
        }
    }

    //Consulta pelo nome do Usuario
    T ConsultaEmprestimo(string chave){
        int posicao = funcaoHash(chave);
        aux = HashTable[posicao];

        if(!(HashTable[posicao]->marcador == "L")){
            while(chave.compare(aux->item.getLivro().getTitulo()) != 0 || aux->next != NULL){
                aux = aux->next;
            }
            return aux->item;
        }
        else{
            return T();
        }
    }

    //Consulta livro por Area
    //Lista livros por area
    vector<T> ConsultaLivroPorArea(string chave, Controle &controle){
        vector<T> lista;
        int atr = 0;
        int cmp = 0;
        int opr = 0;
        atr+=6;
        cmp++;
        for(int i = 0; i < tamanhoTabela;i++){
            aux = HashTable[i];
            atr++;
            cmp+=2;
            if(!(HashTable[i]->marcador == "L")){
                if((aux->item.getArea().getNome()).find(chave.c_str()) != string::npos){
                    lista.push_back(aux->item);
                }
                while(aux->next != NULL){
                    cmp++;
                    aux = aux->next;
                    atr++;
                    cmp++;
                    if((aux->item.getArea().getNome()).find(chave.c_str()) != string::npos){
                        lista.push_back(aux->item);
                        atr++;
                        opr++;
                    }
                }
            }
        }
        controle.setAtribuicoes(atr);
        controle.setComparacoes(cmp);
        controle.setOperacoes(1);

        return lista;
    }

    //Consulta livro por autor
    vector<T> ConsultaLivroPorAutor(string chave, Controle &controle){
        vector<T> lista;
        int atr = 0;
        int cmp = 0;
        int opr = 0;
        atr+=6;
        cmp++;
        for(int i = 0; i < tamanhoTabela;i++){
            aux = HashTable[i];
            atr++;
            cmp+=2;
            if(!(HashTable[i]->marcador == "L")){
                if((aux->item.getAutor()).find(chave.c_str()) != string::npos){
                    lista.push_back(aux->item);
                }
                while(aux->next != NULL){
                    cmp++;
                    aux = aux->next;
                    atr++;
                    cmp++;
                    if((aux->item.getAutor()).find(chave.c_str()) != string::npos){
                        lista.push_back(aux->item);
                        atr++;
                        opr++;
                    }
                }
            }
        }
        controle.setAtribuicoes(atr);
        controle.setComparacoes(cmp);
        controle.setOperacoes(1);

        return lista;
    }

    //Consulta livro por editora
    vector<T> ConsultaLivroPorEditora(string chave, Controle &controle){
        vector<T> lista;
        int atr = 0;
        int cmp = 0;
        int opr = 0;
        atr+=6;
        cmp++;
        for(int i = 0; i < tamanhoTabela;i++){
            aux = HashTable[i];
            atr++;
            cmp+=2;
            if(!(HashTable[i]->marcador == "L")){
                if((aux->item.getEditora()).find(chave.c_str()) != string::npos){
                    lista.push_back(aux->item);
                }
                while(aux->next != NULL){
                    cmp++;
                    aux = aux->next;
                    atr++;
                    cmp++;
                    if((aux->item.getEditora()).find(chave.c_str()) != string::npos){
                        lista.push_back(aux->item);
                        atr++;
                        opr++;
                    }
                }
            }
        }
        controle.setAtribuicoes(atr);
        controle.setComparacoes(cmp);
        controle.setOperacoes(1);

        return lista;
    }

    //Consulta livro por titulo
    vector<T> ConsultaLivroPorTitulo(string chave, Controle &controle){
        vector<T> lista;
        int atr = 0;
        int cmp = 0;
        int opr = 0;
        atr+=6;
        cmp++;
        for(int i = 0; i < tamanhoTabela;i++){
            aux = HashTable[i];
            atr++;
            cmp+=2;
            if(!(HashTable[i]->marcador == "L")){
                if((aux->item.getTitulo()).find(chave.c_str()) != string::npos){
                    lista.push_back(aux->item);
                }
                while(aux->next != NULL){
                    cmp++;
                    aux = aux->next;
                    atr++;
                    cmp++;
                    if((aux->item.getTitulo()).find(chave.c_str()) != string::npos){
                        lista.push_back(aux->item);
                        atr++;
                        opr++;
                    }
                }
            }
        }
        controle.setAtribuicoes(atr);
        controle.setComparacoes(cmp);
        controle.setOperacoes(1);

        return lista;
    }

    //Lista os livros pelas areas
    vector<T> ListaLivrosPorArea(string chave, Controle &controle){
        vector<T> lista;
        int atr = 0;
        int cmp = 0;
        int opr = 0;
        atr+=6;
        cmp++;
        for(int i = 0; i < tamanhoTabela;i++){
            aux = HashTable[i];
            atr++;
            cmp+=2;
            if(!(HashTable[i]->marcador == "L")){
                if((aux->item.getArea().getNome()).compare(chave.c_str()) == 0){
                    lista.push_back(aux->item);
                }
                while(aux->next != NULL){
                    cmp++;
                    aux = aux->next;
                    atr++;
                    cmp++;
                    if((aux->item.getArea().getNome()).compare(chave.c_str()) == 0){
                        lista.push_back(aux->item);
                        atr++;
                        opr++;
                    }
                }
            }
        }
        controle.setAtribuicoes(atr);
        controle.setComparacoes(cmp);
        controle.setOperacoes(1);

        return lista;
    }


    //Consulta emprestimos por data
    vector<T> ConsultaEmprestimosPorData(string chave, Controle &controle){
        vector<T> lista;
        int atr = 0;
        int cmp = 0;
        int opr = 0;
        atr+=6;
        cmp++;
        for(int i = 0; i < tamanhoTabela;i++){
            aux = HashTable[i];
            atr++;
            cmp+=2;
            if(!(HashTable[i]->marcador == "L")){
                if( (Data::montaStringData(aux->item.getData()).find(chave.c_str()) ) != string::npos){
                    lista.push_back(aux->item);
                }
                while(aux->next != NULL){
                    cmp++;
                    aux = aux->next;
                    atr++;
                    cmp++;
                    if( (Data::montaStringData(aux->item.getData()).find(chave.c_str()) ) != string::npos){
                        lista.push_back(aux->item);
                        atr++;
                        opr++;
                    }
                }
            }
        }
        controle.setAtribuicoes(atr);
        controle.setComparacoes(cmp);
        controle.setOperacoes(1);

        return lista;
    }

    //Consulta emprestimos por funcionario
    vector<T> ConsultaEmprestimosPorFuncionario(string chave, Controle &controle){
        vector<T> lista;
        int atr = 0;
        int cmp = 0;
        int opr = 0;
        atr+=6;
        cmp++;
        for(int i = 0; i < tamanhoTabela;i++){
            aux = HashTable[i];
            atr++;
            cmp+=2;
            if(!(HashTable[i]->marcador == "L")){
                if((aux->item.getFuncionario().getNome()).find(chave.c_str()) != string::npos){
                    lista.push_back(aux->item);
                }
                while(aux->next != NULL){
                    cmp++;
                    aux = aux->next;
                    atr++;
                    cmp++;
                    if((aux->item.getFuncionario().getNome()).find(chave.c_str()) != string::npos){
                        lista.push_back(aux->item);
                        atr++;
                        opr++;
                    }
                }
            }
        }
        controle.setAtribuicoes(atr);
        controle.setComparacoes(cmp);
        controle.setOperacoes(1);

        return lista;
    }

    //Consulta emprestimos por usuario
    vector<T> ConsultaEmprestimosPorUsuario(string chave, Controle &controle){
        vector<T> lista;
        int atr = 0;
        int cmp = 0;
        int opr = 0;
        atr+=6;
        cmp++;
        for(int i = 0; i < tamanhoTabela;i++){
            aux = HashTable[i];
            atr++;
            cmp+=2;
            if(!(HashTable[i]->marcador == "L")){
                if((aux->item.getUsuario().getNome()).find(chave.c_str()) != string::npos){
                    lista.push_back(aux->item);
                }
                while(aux->next != NULL){
                    cmp++;
                    aux = aux->next;
                    atr++;
                    cmp++;
                    if((aux->item.getUsuario().getNome()).find(chave.c_str()) != string::npos){
                        lista.push_back(aux->item);
                        atr++;
                        opr++;
                    }
                }
            }
        }
        controle.setAtribuicoes(atr);
        controle.setComparacoes(cmp);
        controle.setOperacoes(1);

        return lista;
    }



    //Consulta usuarios por nome
    vector<T> ConsultaUsuarioFuncionarioPorNome(string chave, Controle &controle){
        vector<T> lista;
        int atr = 0;
        int cmp = 0;
        int opr = 0;
        atr+=6;
        cmp++;
        for(int i = 0; i < tamanhoTabela;i++){
            aux = HashTable[i];
            atr++;
            cmp+=2;
            if(!(HashTable[i]->marcador == "L")){
                if((aux->item.getNome()).find(chave.c_str()) != string::npos){
                    lista.push_back(aux->item);
                }
                while(aux->next != NULL){
                    cmp++;
                    aux = aux->next;
                    atr++;
                    cmp++;
                    if((aux->item.getNome()).find(chave.c_str()) != string::npos){
                        lista.push_back(aux->item);
                        atr++;
                        opr++;
                    }
                }
            }
        }
        controle.setAtribuicoes(atr);
        controle.setComparacoes(cmp);
        controle.setOperacoes(1);

        return lista;
    }


    vector<T> ListaTodos(){
        vector<T> lista;

        for(int i = 0; i < tamanhoTabela;i++){
            aux = HashTable[i];

            if(!(HashTable[i]->marcador == "L")){
                lista.push_back(aux->item);
                while(aux->next != NULL){
                    aux = aux->next;
                    lista.push_back(aux->item);
                }
            }
        }
        return lista;
    }

    vector<T> ListaTodos(Controle controle){
        int atr = 0;
        int cmp = 0;
        int i;

        vector<T> lista;

        for(int i = 0; i < tamanhoTabela;i++){

            aux = HashTable[i]; atr++;

            cmp++;
            if(!(HashTable[i]->marcador == "L")){

                lista.push_back(aux->item);
                cmp++;
                atr++;
                while(aux->next != NULL){
                    cmp++;
                    aux = aux->next;
                    lista.push_back(aux->item);
                    atr+2;
                }
            }
        }

        controle.setAtribuicoes(atr);
        controle.setComparacoes(cmp);
        controle.setOperacoes(1);

        return lista;
    }

    bool Edita(T item, string chave){
        int posicao = funcaoHash(chave);
        aux = HashTable[posicao];

        if(!(HashTable[posicao]->marcador == "L")){

            while(aux->item.getCodigo() == item.getCodigo()){
                HashTable[posicao]->item = item;
                return true;
            }
        }
        else{
            return false;
        }
    }

    //Deleta usa a ID e a chave do objeto!
    bool Deleta(int codigo, string chave){

        int posicao = funcaoHash(chave);

        tipoCelula auxDel1;
        tipoCelula auxDel2;

        if(HashTable[posicao]->marcador == "L"){
            return false;
        }

        else if(HashTable[posicao]->item.getCodigo() == codigo && HashTable[posicao]->next == NULL){
            HashTable[posicao]->marcador = "L";
        }
        else if(HashTable[posicao]->item.getCodigo() == codigo){

            aux = HashTable[posicao];
            HashTable[posicao] = HashTable[posicao]->next;
            delete aux;
            return false;
        }


        else{
            auxDel1 = HashTable[posicao]->next;
            auxDel2 = HashTable[posicao];

            while(auxDel1 != NULL && auxDel1->item.getCodigo() != codigo){
                auxDel2 = auxDel1;
                auxDel1 = auxDel1->next;
            }

            if(auxDel1 == NULL){
                return false;
            }
            else{
                aux = auxDel1;
                auxDel1 = auxDel1->next;
                auxDel2->next = auxDel1;

                delete aux;
                return true;
            }
        }
    }
};

#endif // HASHABERTO_H
