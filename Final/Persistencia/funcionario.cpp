#include "funcionario.h"

Funcionario::Funcionario(){

}

Funcionario::Funcionario(int codigo, string nome, Data admissao, string CTPS, string login, string senha){

    this->setNome(nome);
    this->setCodigo(codigo);
    this->setAdmissao(admissao);
    this->setCTPS(CTPS);
    this->setLogin(login);
    this->setSenha(senha);

}


void Funcionario::setAdmissao(Data admissao){
    this->admissao = admissao;
}


void Funcionario::setNome(string nome){
    this->nome = nome;
}

string Funcionario::getNome(){
    return nome;
}

void Funcionario::setCodigo(int codigo){
    this->codigo = codigo;
}

int Funcionario::getCodigo(){
    return codigo;
}

void Funcionario::setCTPS(string CTPS){
    this->CTPS = CTPS;
}

void Funcionario::setLogin(string login){
    this->login = login;
}

void Funcionario::setSenha(string senha){
    this->senha = senha;
}

Data Funcionario::getAdmissao(){
    return admissao;
}

string Funcionario::getCTPS(){
    return CTPS;
}

string Funcionario::getLogin(){
    return login;
}

string Funcionario::getSenha(){
    return senha;
}
