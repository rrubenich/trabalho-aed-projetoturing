#include "livro.h"


//Construtores
Livro::Livro(){
    setCodigo(0);
    setTitulo("");
    setAutor("");
    setEditora("");
    setAno(0);
    setNumeroExemplares(0);
    setEdicao(0);
}

Livro::Livro(int codigo, string titulo, string autor, string editora, int ano, int numeroExemplares, Area area, int edicao){
    setCodigo(codigo);
    setTitulo(titulo);
    setAutor(autor);
    setEditora(editora);
    setAno(ano);
    setNumeroExemplares(numeroExemplares);
    setArea(area);
    setEdicao(edicao);
}


//Encapsulamentos
void Livro::setCodigo(int codigo){
    this->codigo = codigo;
}

void Livro::setTitulo(string titulo){
    this->titulo = titulo;
}

void Livro::setAutor(string autor){
    this->autor = autor;
}

void Livro::setEditora(string editora){
    this->editora = editora;
}

void Livro::setAno(int ano){
    this->ano = ano;
}

void Livro::setNumeroExemplares(int numeroExemplares){
    this->numeroExemplares = numeroExemplares;
}

void Livro::setArea(Area area){
    this->area = area;
}

void Livro::setEdicao(int edicao){
    this->edicao = edicao;
}

int Livro::getCodigo(){
    return codigo;
}

string Livro::getTitulo(){
    return titulo;
}

string Livro::getAutor(){
    return autor;
}

string Livro::getEditora(){
    return editora;
}

int Livro::getAno(){
    return ano;
}

int Livro::getNumeroExemplares(){
    return numeroExemplares;
}

Area Livro::getArea(){
    return area;
}

int Livro::getEdicao(){
    return edicao;
}
