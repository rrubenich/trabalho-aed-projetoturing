#include "controle.h"

Controle::Controle(){
    this->atribuicoes = 0;
    this->comparacoes = 0;
    this->operacoes = 0;
}

Controle::Controle(string funcao, int operacoes, int comparacoes, int atribuicoes){
    this->funcao = funcao;
    this->atribuicoes = atribuicoes;
    this->comparacoes = comparacoes;
    this->operacoes = operacoes;
}

void Controle::setFuncao(string funcao){
    this->funcao = funcao;
}

void Controle::setOperacoes(int operacoes){
    this->operacoes += operacoes;
}

void Controle::setComparacoes(int comparacoes){
    this->comparacoes += comparacoes;
}

void Controle::setAtribuicoes(int atribuicoes){
    this->atribuicoes += atribuicoes;
}

string Controle::getFuncao(){
    return funcao;
}

unsigned long int Controle::getOperacoes(){
    return operacoes;
}

unsigned long int Controle::getComparacoes(){
    return comparacoes;
}

unsigned long int Controle::getAtribuicoes(){
    return atribuicoes;
}
