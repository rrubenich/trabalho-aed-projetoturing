#ifndef USUARIO_H
#define USUARIO_H

#include <string>
#include <Persistencia/endereco.h>

using namespace std;

class Usuario{

public:
    Usuario();
    Usuario(int codigo, string nome, string RG, string CPF, string email, string telefone, Endereco endereco);

    void setCodigo(int codigo);
    void setNome(string nome);
    void setRG(string RG);
    void setCPF(string CPF);
    void setEmail(string email);
    void setTelefone(string telefone);
    void setEndereco(Endereco endereco);

    int getCodigo();
    string getNome();
    string getRG();
    string getCPF();
    string getEmail();
    string getTelefone();
    Endereco getEndereco();
    string toString();

private:
    int codigo;
    string nome;
    string RG;
    string CPF;
    string email;
    string telefone;
    Endereco endereco;
};

#endif // PESSOA_H
