#include "emprestimo.h"

Emprestimo::Emprestimo(){

}

Emprestimo::Emprestimo(int codigo, Usuario usuario, Livro livro, Funcionario funcionario, Data data){
    setCodigo(codigo);
    setUsuario(usuario);
    setLivro(livro);
    setFuncionario(funcionario);
    setData(data);
}

void Emprestimo::setUsuario(Usuario usuario){
    this->usuario = usuario;
}

void Emprestimo::setLivro(Livro livro){
    this->livro = livro;
}

void Emprestimo::setFuncionario(Funcionario funcionario){
    this->funcionario = funcionario;
}

void Emprestimo::setData(Data data){
    this->data = data;
}

void Emprestimo::setCodigo(int codigo){
    this->codigo = codigo;
}

Usuario Emprestimo::getUsuario(){
    return usuario;
}

Livro Emprestimo::getLivro(){
    return livro;
}

Funcionario Emprestimo::getFuncionario(){
    return funcionario;
}

int Emprestimo::getCodigo(){
    return codigo;
}

Data Emprestimo::getData(){
    return data;
}
