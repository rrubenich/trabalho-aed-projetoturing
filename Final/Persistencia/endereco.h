#ifndef ENDERECO_H
#define ENDERECO_H

#include <string>

using namespace std;

class Endereco{
public:
    Endereco();

    Endereco(string endereco);
    void setEndereco(string bairro);
    string getEndereco();

   /*
    * Os métodos abaixo foram feitos para uma estrutura de endereços detalhada,
    * porém, só após verificou-se que o atributo endereço é fornecido só como
    * uma string, então, a classe endereço possui somente 1 atributo, ainda
    * respeitando o conceito de entidade em orientação a objetos. A estrutura
    * restante foi comentada para caso haja modificações futuras.
    *
    Endereco(string bairro, string cidade, string rua, string cep, int numero);

    void setBairro(string bairro);
    void setCidade(string cidade);
    void setRua(string rua);
    void setCep(string cep);
    void setNumero(int numero);

    string getBairro();
    string getCidade();
    string getRua();
    string getCep();
    int getNumero();
    */

private:

    string endereco;

    /*
    string bairro;
    string cidade;
    string rua;
    string cep;
    int    numero;
    */
};

#endif // ENDERECO_H
