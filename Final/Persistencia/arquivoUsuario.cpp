
#include "arquivoUsuario.h"
#include <iostream>
#include <string>
#include <fstream>
#include <Persistencia/usuario.h>
#include <Persistencia/endereco.h>

arquivoUsuario::arquivoUsuario(){

}

void arquivoUsuario::setHeader(string header){
    this->header = header;
}

string arquivoUsuario::getHeader(){
    return header;
}

void arquivoUsuario::leArquivo(HashAberto<Usuario> &usuarios, Controle &controle){
    fstream arquivo;
    int i = 0, j= 1;
    string header;
    char ch;
    string coluna[6];


    arquivo.open("Arquivos/Arquivo II - usuario.csv");

    if(arquivo.is_open()){
        getline(arquivo,header,(char)13);
        setHeader(header);

        while(arquivo.get(ch)){
            if(i == 5){
                getline(arquivo,coluna[i],(char)13);
                coluna[i] = ch+coluna[i];

                Endereco end(coluna[3]);
                Usuario novo(j, coluna[0], coluna[1], coluna[2], coluna[4], coluna[5], end);
                usuarios.Insere(novo,coluna[0] ,controle);
                i = -1;
                j++;
            }else if(ch == (char)34){
                getline(arquivo,coluna[i],(char)34);
                arquivo.get(ch);
            }else{
                getline(arquivo,coluna[i],(char)44);
                coluna[i] = ch + coluna[i];
            }
            i++;
        }


    }

    arquivo.close();
}

void arquivoUsuario::gravaArquivo(HashAberto<Usuario> &usuarios){
    fstream arquivo;
    vector<Usuario> vetorUsuarios = usuarios.ListaTodos();

    arquivo.open("Arquivos/Arquivo II - usuario.csv", ios::out);
    arquivo << getHeader();

    int tam = vetorUsuarios.size();
    for(int i = 0; i < tam; i++){
        arquivo << (char)13;
        arquivo << vetorUsuarios.at(i).getNome() << (char)44;
        arquivo << vetorUsuarios.at(i).getRG() << (char)44;
        arquivo << vetorUsuarios.at(i).getCPF() << (char)44;
        arquivo << (char)34 << vetorUsuarios.at(i).getEndereco().getEndereco() << (char)34 << (char)44;
        arquivo << vetorUsuarios.at(i).getEmail() << (char)44;
        arquivo << vetorUsuarios.at(i).getTelefone();
    }

    arquivo.close();

}
