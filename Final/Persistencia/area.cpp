#include <Persistencia/area.h>

Area::Area(){

}

Area::Area(int codigo, string nome){
    setCodigo(codigo);
    setNome(nome);
}

void Area::setCodigo(int codigo){
    this->codigo = codigo;
}

void Area::setNome(string nome){
    this->nome = nome;
}

int Area::getCodigo(){
    return codigo;
}

string Area::getNome(){
    return nome;
}
