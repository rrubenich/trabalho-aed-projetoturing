#ifndef AREA_H
#define AREA_H

#include <string>

using namespace std;

class Area{
private:
    int codigo;
    string nome;

public:
    Area();
    Area(int codigo, string nome);

    void setCodigo(int codigo);
    void setNome(string nome);

    int getCodigo();
    string getNome();
};
#endif // HASHCELULA_H
