#ifndef ARQUIVOCONTROLE_H
#define ARQUIVOCONTROLE_H

#include <Persistencia/controle.h>
#include <Persistencia/lista.h>

class arquivoControle{
public:
    arquivoControle();
    void gravaArquivo(Lista<Controle> &controles);
    string getHeader();
private:
    const string header = ",Numero de Execucoes,Total de Comparacoes,Total de Atribuicoes";
};

#endif // ARQUIVOCONTROLE_H
