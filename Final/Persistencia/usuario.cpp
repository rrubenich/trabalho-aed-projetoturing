#include "usuario.h"

Usuario::Usuario(){
    setCodigo(0);
    setNome("");
    setRG("");
    setCPF("");
    setEmail("");
    setTelefone("");
}

Usuario::Usuario(int codigo, string nome, string RG, string CPF, string email, string telefone, Endereco endereco){
    setCodigo(codigo);
    setNome(nome);
    setRG(RG);
    setCPF(CPF);
    setEmail(email);
    setTelefone(telefone);
    setEndereco(endereco);
}

void Usuario::setCodigo(int codigo){
    this->codigo = codigo;
}

void Usuario::setNome(string nome){
    this->nome = nome;
}

void Usuario::setRG(string RG){
    this->RG = RG;
}

void Usuario::setCPF(string CPF){
    this->CPF = CPF;
}

void Usuario::setEmail(string email){
    this->email = email;
}

void Usuario::setTelefone(string telefone){
    this->telefone = telefone;
}

void Usuario::setEndereco(Endereco endereco){
    this->endereco = endereco;
}

int Usuario::getCodigo(){
    return codigo;
}

string Usuario::getNome(){
    return nome;
}
string Usuario::getRG(){
    return RG;
}

string Usuario::getCPF(){
    return CPF;
}

string Usuario::getEmail(){
    return email;
}

string Usuario::getTelefone(){
    return telefone;
}

Endereco Usuario::getEndereco(){
    return endereco;
}

string Usuario::toString(){
    return this->getCodigo() + ", " + this->getNome() + ", " +
            this->getRG() + ", " + this->getCPF() + ", " +
            this->getEmail() + ", " + this->getEndereco().getEndereco();
}
