#include "log.h"

Log::Log(){

}


Log::Log(Usuario usuario, Livro livro, Funcionario funcionario, Data data, Data registro){
    setUsuario(usuario);
    setLivro(livro);
    setFuncionario(funcionario);
    setData(data);
    setRegistro(registro);
}

void Log::setUsuario(Usuario usuario){
    this->usuario = usuario;
}

void Log::setLivro(Livro livro){
    this->livro = livro;
}

void Log::setFuncionario(Funcionario funcionario){
    this->funcionario = funcionario;
}

void Log::setData(Data data){
    this->data = data;
}

void Log::setRegistro(Data registro){
    this->registro = registro;
}

Usuario Log::getUsuario(){
    return usuario;
}

Livro Log::getLivro(){
    return livro;
}

Funcionario Log::getFuncionario(){
    return funcionario;
}

Data Log::getData(){
    return data;
}

Data Log::getRegistro(){
    return registro;
}
