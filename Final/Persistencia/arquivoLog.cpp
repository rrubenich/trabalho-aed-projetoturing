#include "arquivoLog.h"
#include <iostream>
#include <string>
#include <fstream>
#include <Persistencia/livro.h>

ArquivoLog::ArquivoLog(){

}

string ArquivoLog::getHeader(){
    return header;
}

void ArquivoLog::gravaArquivo(Log log){
    fstream arquivo;

    Data data;

    string strData, registro;

    arquivo.open("Arquivos/Arquivo V - log.csv", ios::out | ios::app);

    strData = data.montaStringData(log.getData());
    registro = data.montaStringData(log.getRegistro());

    arquivo << (char)13;
    arquivo << log.getUsuario().getNome() << (char)44;
    arquivo << log.getLivro().getTitulo() << (char)44;
    arquivo << log.getFuncionario().getLogin() << (char)44;
    arquivo << strData << (char)44;
    arquivo << registro;

    arquivo.close();

}

