#include "framegerenciarlivro.h"
#include "ui_framegerenciarlivro.h"
#include "Controle/telas.h"

QTableView *tvResultadosLivro;
QStandardItemModel *modelLivro;
QComboBox *cbFiltroLivro;
QComboBox *cbFiltroLivroAreas;
QLineEdit *leFiltroBuscaLivro;

FrameGerenciarLivro::FrameGerenciarLivro(QWidget *parent):QFrame(parent),ui(new Ui::FrameGerenciarLivro){
    ui->setupUi(this);
    this->setGeometry(0,20,800,580);

    //Insere colunas tabela
    modelLivro = new QStandardItemModel(0,6,this); //0 Rows and 4 Columns
    modelLivro->setHorizontalHeaderItem(0, new QStandardItem(QString("ID")));
    modelLivro->setHorizontalHeaderItem(1, new QStandardItem(QString("Livro")));
    modelLivro->setHorizontalHeaderItem(2, new QStandardItem(QString("Autor")));
    modelLivro->setHorizontalHeaderItem(3, new QStandardItem(QString("Quantidade")));
    modelLivro->setHorizontalHeaderItem(4, new QStandardItem(QString("Editora")));
    modelLivro->setHorizontalHeaderItem(5, new QStandardItem(QString("Área")));

    //Popula tabela
    tvResultadosLivro = ui->tvResultados;
    tvResultadosLivro->setModel(modelLivro);

    //Muda larguras colunas
    tvResultadosLivro->setColumnWidth(0, 45);
    tvResultadosLivro->setColumnWidth(1, 300);
    tvResultadosLivro->setColumnWidth(2, 100);
    tvResultadosLivro->setColumnWidth(3, 70);
    tvResultadosLivro->setColumnWidth(4, 100);
    tvResultadosLivro->setColumnWidth(5, 100);

    //Popula combobox
    cbFiltroLivro = ui->cbFiltroBusca;

    cbFiltroLivro->addItem("Editora");
    cbFiltroLivro->addItem("Área");
    cbFiltroLivro->addItem("Autor");
    cbFiltroLivro->addItem("Título");

    cbFiltroLivroAreas = ui->cbFiltroLivroAreas;
    cbFiltroLivroAreas->setEnabled(false);

    leFiltroBuscaLivro = ui->leCampoBusca;
}

QList<QStandardItem*> FrameGerenciarLivro::montaLinhaTabelaLivro(Livro l){
    Conversoes c;
    QList<QStandardItem*> newRow;

    QStandardItem *Col1 = new QStandardItem(QString(c.converteIntParaString(l.getCodigo()).c_str()));
    newRow.append(Col1);

    QStandardItem *Col2 = new QStandardItem(QString(l.getTitulo().c_str()));
    newRow.append(Col2);

    QStandardItem *Col3 = new QStandardItem(QString(l.getAutor().c_str()));
    newRow.append(Col3);

    QStandardItem *Col4 = new QStandardItem(QString(c.converteIntParaString(l.getNumeroExemplares()).c_str()));
    newRow.append(Col4);

    QStandardItem *Col5 = new QStandardItem(QString(l.getEditora().c_str()));
    newRow.append(Col5);

    QStandardItem *Col6 = new QStandardItem(QString(l.getArea().getNome().c_str()));
    newRow.append(Col6);

    return newRow;
}

void FrameGerenciarLivro::carregarTabelaLivro(vector<Livro> v){
    int linhas = 0;

    modelLivro->removeRows(0,modelLivro->rowCount());

    int tam = v.size();
    for(int i = 0; i < tam; i++){
        Livro l = v[i];

        modelLivro->insertRow(linhas,montaLinhaTabelaLivro(l));
        linhas++;
    }

}

int FrameGerenciarLivro::pegaIDLivroLinhaSelecionada(){
    if(tvResultadosLivro->selectionModel()->hasSelection()){
        Conversoes c;

        int row = tvResultadosLivro->selectionModel()->currentIndex().row();
        int col = 0;
        int idLivro = c.converteStringParaInt(modelLivro->item(row,col)->text().toStdString());

        return idLivro;
    }
    else{
        return -1;
    }
}

void FrameGerenciarLivro::populaComboBoxAreas(){
    vector<Area> listaAreas = Dados::areaHash.ListaTodos();

    int tam = listaAreas.size();
    string areas[11];
    for(int i = 0; i < tam; i++){
        Area a = listaAreas[i];
        areas[a.getCodigo()-1] = a.getNome();
    }

    for(int i = 0; i < tam; i++){
        cbFiltroLivroAreas->addItem(areas[i].c_str());
    }
}

string FrameGerenciarLivro::pegaNomeLivroLinhaSelecionada(){
    if(tvResultadosLivro->selectionModel()->hasSelection()){
        Conversoes c;

        int row = tvResultadosLivro->selectionModel()->currentIndex().row();
        int col = 1;
        string livro = modelLivro->item(row,col)->text().toStdString();

        return livro;
    }
    else{
        return "";
    }
}

void FrameGerenciarLivro::limparCamposGerenciarLivro(int x){
    if(x == 0){
        modelLivro->removeRows(0,modelLivro->rowCount());
        cbFiltroLivro->setCurrentIndex(0);
        leFiltroBuscaLivro->clear();
    }
    else{
        cbFiltroLivro->setCurrentIndex(0);
        leFiltroBuscaLivro->clear();
    }
}

FrameGerenciarLivro::~FrameGerenciarLivro(){
    delete ui;
}

void FrameGerenciarLivro::on_pbCancelar_clicked(){
    this->hide();
}

void FrameGerenciarLivro::on_pbEditarLivro_clicked(){
    int id = pegaIDLivroLinhaSelecionada();
    string livro = pegaNomeLivroLinhaSelecionada();

    if(id != -1){
        Livro l = Dados::livroHash.ConsultaLivro(livro);
        Telas::frameCadastrarLivro->trocaTituloTelaLivro("Editar Livro");
        Telas::frameCadastrarLivro->recebeDados(l);

        this->hide();
        Telas::frameCadastrarLivro->show();
    }
    else{
        Dados::popupMessage->mensagem("Erro!","Tabela não possui linha selecionada!");
    }
}

void FrameGerenciarLivro::on_pbMaisInfos_clicked(){
    int id = pegaIDLivroLinhaSelecionada();
    string livro = pegaNomeLivroLinhaSelecionada();

    if(id != -1){
        Livro l = Dados::livroHash.ConsultaLivro(livro);
        Telas::frameCadastrarLivro->trocaTituloTelaLivro("Informações Livro");
        Telas::frameCadastrarLivro->recebeDados(l);

        this->hide();
        Telas::frameCadastrarLivro->show();
    }
    else{
        Dados::popupMessage->mensagem("Erro!","Tabela não possui linha selecionada!");
    }
}

void FrameGerenciarLivro::on_pbExcluirLivro_clicked(){
    int id = pegaIDLivroLinhaSelecionada();
    string livro = pegaNomeLivroLinhaSelecionada();

    if(id != -1){
        Livro l = Dados::livroHash.ConsultaLivro(livro);
        Dados::livroHash.Deleta(l.getCodigo(),l.getTitulo());
        carregarTabelaLivro(Dados::livroHash.ListaTodos());
    }
    else{
        Dados::popupMessage->mensagem("Erro!","Tabela não possui linha selecionada!");
    }
}

void FrameGerenciarLivro::on_pbBuscar_clicked(){
    string busca = leFiltroBuscaLivro->text().toStdString();

    if (cbFiltroLivro->currentIndex() == 2) {
        //autor
        if(!busca.empty()){
            vector<Livro> v = Dados::livroHash.ConsultaLivroPorAutor(busca,Dados::controleConsultaLivroAutor);
            carregarTabelaLivro(v);
        }else{
            vector<Livro> v = Dados::livroHash.ListaTodos();
            carregarTabelaLivro(v);
        }
    }
    if (cbFiltroLivro->currentIndex() == 0) {
        //editora
        if(!busca.empty()){
            vector<Livro> v = Dados::livroHash.ConsultaLivroPorEditora(busca,Dados::controleConsultaLivroEditora);
            carregarTabelaLivro(v);
        }else{
            vector<Livro> v = Dados::livroHash.ListaTodos();
            carregarTabelaLivro(v);
        }
    }
    if (cbFiltroLivro->currentIndex() == 3) {
        //titulo
        if(!busca.empty()){
            vector<Livro> v = Dados::livroHash.ConsultaLivroPorTitulo(busca,Dados::controleConsultaLivroTitulo);
            carregarTabelaLivro(v);
        }else{
            vector<Livro> v = Dados::livroHash.ListaTodos();
            carregarTabelaLivro(v);
        }
    }
    if (cbFiltroLivro->currentIndex() == 1) {
        if(busca.empty()){
            //lista livros por area
            string area = cbFiltroLivroAreas->currentText().toStdString();
            vector<Livro> v = Dados::livroHash.ListaLivrosPorArea(area,Dados::controleListaLivrosArea);
            carregarTabelaLivro(v);
        }
        else{
            //busca livros por area
            vector<Livro> v = Dados::livroHash.ConsultaLivroPorArea(busca,Dados::controleConsultaLivroArea);
            carregarTabelaLivro(v);
        }

    }
}

void FrameGerenciarLivro::on_cbFiltroBusca_activated(int index){
    if(index == 1){
        cbFiltroLivroAreas->setEnabled(true);
    }
    else{
        cbFiltroLivroAreas->setEnabled(false);
    }
}

void FrameGerenciarLivro::on_cbFiltroBusca_activated(const QString &arg1)
{

}

void FrameGerenciarLivro::on_cbFiltroLivroAreas_activated(const QString &arg1)
{

}
