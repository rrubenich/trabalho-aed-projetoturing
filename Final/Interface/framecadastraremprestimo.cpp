#include "framecadastraremprestimo.h"
#include "ui_framecadastraremprestimo.h"
#include "Controle/telagerenciar.h"

using namespace std;

QLineEdit *leCliente;
QLineEdit *leLivro;
QLineEdit *leCodigoEmprestimo;
QLineEdit *leFuncionarioLogado;
QLineEdit *leDataEmprestimo;
QLineEdit *leDataDevolucao;

QLabel *lblTituloTelaEmprestimo;
QPushButton *pbRealizarEmprestimo;
QPushButton *pbLimparCamposEmprestimo;

FrameCadastrarEmprestimo::FrameCadastrarEmprestimo(QWidget *parent) :QFrame(parent),ui(new Ui::FrameCadastrarEmprestimo){
    ui->setupUi(this);
    this->setGeometry(0,20,800,580);

    leCliente = ui->leCliente;
    leLivro = ui->leLivro;
    leCodigoEmprestimo = ui->leCodigoEmprestimo;
    leFuncionarioLogado = ui->leFuncionarioLogado;
    leDataEmprestimo = ui->leDataEmprestimo;
    leDataDevolucao = ui->leDataDevolucao;

    lblTituloTelaEmprestimo = ui->lblTitulo;
    pbRealizarEmprestimo = ui->pbRealizarEmprestimo;
    pbLimparCamposEmprestimo = ui->pbLimparCampos;
}

FrameCadastrarEmprestimo::~FrameCadastrarEmprestimo(){
    delete ui;
}

void FrameCadastrarEmprestimo::limparCamposEmprestimo(int x){
    if(x == 0){
        leCodigoEmprestimo->clear();
        leFuncionarioLogado->clear();
        leDataEmprestimo->clear();
        leDataDevolucao->clear();
        leCliente->clear();
        leLivro->clear();
    }
    else{
        leCliente->clear();
        leLivro->clear();
    }
}

void FrameCadastrarEmprestimo::trocaTituloTelaEmprestimo(string txt){
    lblTituloTelaEmprestimo->setText(QString::fromStdString(txt));
    pbRealizarEmprestimo->setText(QString::fromStdString(txt));

    if(txt.compare("Informações Empréstimo") == 0){
        pbRealizarEmprestimo->hide();
        pbLimparCamposEmprestimo->hide();
        leCliente->setEnabled(false);
        leLivro->setEnabled(false);
    }
    else{
        pbRealizarEmprestimo->show();
        pbLimparCamposEmprestimo->show();
        leCliente->setEnabled(true);
        leLivro->setEnabled(true);
    }

}

void FrameCadastrarEmprestimo::on_pbLimparCampos_clicked(){
    limparCamposEmprestimo(1);
}

void FrameCadastrarEmprestimo::on_pbCancelar_clicked(){
    limparCamposEmprestimo(0);
    if(lblTituloTelaEmprestimo->text().toStdString().compare("Realizar Empréstimo") == 0){
        this->hide();
    }
    else{
        this->hide();
        TelaGerenciar::frameGerenciarEmprestimo->show();
    }

}

void FrameCadastrarEmprestimo::recebeDados(Emprestimo e){
    limparCamposEmprestimo(0);
    Conversoes c;

    //Colando string em QLineEdit
    leFuncionarioLogado->setText(e.getFuncionario().getLogin().c_str());
    leCliente->setText(QString::fromStdString(e.getUsuario().getNome().c_str()));
    leLivro->setText(QString::fromStdString(e.getLivro().getTitulo()));
    leCodigoEmprestimo->setText(QString::fromStdString(c.converteIntParaString(e.getCodigo())));

    string dtEmprestimo = Data::montaStringData(e.getData());
    leDataEmprestimo->setText(QString::fromStdString(dtEmprestimo));

    //Tratar data
    string dtDevolucao = Data::montaStringDataDevolucao(e.getData(),5);
    leDataDevolucao->setText(QString::fromStdString(dtDevolucao));
}

Emprestimo FrameCadastrarEmprestimo::retiraDados(){
    Conversoes c;

    string cliente = leCliente->text().toStdString().c_str();
    string livro = leLivro->text().toStdString().c_str();
    string funcionario = leFuncionarioLogado->text().toStdString().c_str();

    Livro l = Dados::livroHash.ConsultaLivro(livro);
    Usuario u = Dados::usuarioHash.ConsultaUsuario(cliente);
    Funcionario f = Dados::funcionarioHash.ConsultaFuncionario(funcionario);

    int emprestimo = c.converteStringParaInt(leCodigoEmprestimo->text().toStdString().c_str());
    Data data = Data::montaDataString(leDataEmprestimo->text().toStdString());

    Emprestimo e = Emprestimo(emprestimo,u,l,f,data);

    return e;
}

void FrameCadastrarEmprestimo::on_pbRealizarEmprestimo_clicked(){
    if(!(leCodigoEmprestimo->text().isEmpty()
        && leFuncionarioLogado->text().isEmpty()
        && leDataEmprestimo->text().isEmpty()
        && leDataDevolucao->text().isEmpty()
        && leCliente->text().isEmpty()
        && leLivro->text().isEmpty())){

        Emprestimo e = retiraDados();

        if(e.getLivro().getNumeroExemplares() > 1){
            e.getLivro().setNumeroExemplares(e.getLivro().getNumeroExemplares() - 1);

            if(lblTituloTelaEmprestimo->text().toStdString().compare("Realizar Empréstimo") == 0){
                Dados::emprestimoHash.Insere(e,e.getLivro().getTitulo(),Dados::controleCadastroEmprestimo);

                Log g = Log(e.getUsuario(),e.getLivro(),e.getFuncionario(),e.getData(),e.getData());
                Dados::listaLogs.gravaArquivo(g);

                TelaGerenciar::frameGerenciarEmprestimo->carregarTabelaEmprestimo(Dados::emprestimoHash.ListaTodos());
                this->hide();
                Dados::popupMessage->mensagem("Mensagem!","Empréstimo efetuado com sucesso!");
            }
            else if(lblTituloTelaEmprestimo->text().toStdString().compare("Editar Empréstimo") == 0){
                Dados::emprestimoHash.Edita(e,e.getLivro().getTitulo());
                TelaGerenciar::frameGerenciarEmprestimo->carregarTabelaEmprestimo(Dados::emprestimoHash.ListaTodos());
                this->hide();
                TelaGerenciar::frameGerenciarEmprestimo->show();
                Dados::popupMessage->mensagem("Mensagem!","Empréstimo editado com sucesso!");
            }

        }
        else{
            Dados::popupMessage->mensagem("Erro!","Livro escolhido possui menos de 2 cópias disponíveis!");
        }
    }
    else{
        Dados::popupMessage->mensagem("Erro!","Preencha todos os campos!");
    }
}
