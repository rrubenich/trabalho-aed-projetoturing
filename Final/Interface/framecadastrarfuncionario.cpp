#include "framecadastrarfuncionario.h"
#include "ui_framecadastrarfuncionario.h"
#include "Controle/telagerenciar.h"

QLineEdit *leCodigoFuncionario;
QLineEdit *leNomeFuncionario;
QLineEdit *leDataAdmissaoFuncionario;
QLineEdit *leCtpsFuncionario;
QLineEdit *leLoginFuncionario;
QLineEdit *leSenhaFuncionario;

QLabel *lblTituloTelaFuncionario;
QPushButton *pbCadastrarFuncionario;
QPushButton *pbLimparCamposFuncionario;

FrameCadastrarFuncionario::FrameCadastrarFuncionario(QWidget *parent) :QFrame(parent),ui(new Ui::FrameCadastrarFuncionario){
    ui->setupUi(this);
    this->setGeometry(0,20,800,580);

    lblTituloTelaFuncionario = ui->lblTitulo;
    pbCadastrarFuncionario = ui->pbCadastrarFuncionario;
    pbLimparCamposFuncionario = ui->pbLimparCampos;

    leCodigoFuncionario = ui->leCodigoFuncionario;
    leNomeFuncionario = ui->leNomeFuncionario;
    leDataAdmissaoFuncionario = ui->leDataAdmissaoFuncionario;
    leCtpsFuncionario = ui->leCtpsFuncionario;
    leLoginFuncionario = ui->leLoginFuncionario;
    leSenhaFuncionario = ui->leSenhaFuncionario;
}

FrameCadastrarFuncionario::~FrameCadastrarFuncionario()
{
    delete ui;
}

void FrameCadastrarFuncionario::limparCamposFuncionario(int x){
    if(x == 0){
        leCodigoFuncionario->clear();
        leNomeFuncionario->clear();
        leDataAdmissaoFuncionario->clear();
        leCtpsFuncionario->clear();
        leLoginFuncionario->clear();
        leSenhaFuncionario->clear();
    }
    else{
        leNomeFuncionario->clear();
        leCtpsFuncionario->clear();
        leLoginFuncionario->clear();
        leSenhaFuncionario->clear();
    }
}

void FrameCadastrarFuncionario::trocaTituloTelaFuncionario(string txt){
    lblTituloTelaFuncionario->setText(QString::fromStdString(txt));
    pbCadastrarFuncionario->setText(QString::fromStdString(txt));

    if(txt.compare("Informações Funcionário") == 0){
        pbCadastrarFuncionario->hide();
        pbLimparCamposFuncionario->hide();
        leNomeFuncionario->setEnabled(false);
        leDataAdmissaoFuncionario->setEnabled(false);
        leCtpsFuncionario->setEnabled(false);
        leLoginFuncionario->setEnabled(false);
        leSenhaFuncionario->setEnabled(false);
    }
    else{
        pbCadastrarFuncionario->show();
        pbLimparCamposFuncionario->show();
        leNomeFuncionario->setEnabled(true);
        leDataAdmissaoFuncionario->setEnabled(true);
        leCtpsFuncionario->setEnabled(true);
        leLoginFuncionario->setEnabled(true);
        leSenhaFuncionario->setEnabled(true);
    }
}

void FrameCadastrarFuncionario::on_pbLimparCampos_clicked(){
    limparCamposFuncionario(1);
}

void FrameCadastrarFuncionario::on_pbCancelar_clicked(){
    limparCamposFuncionario(0);

    if(lblTituloTelaFuncionario->text().toStdString().compare("Cadastrar Funcionário") == 0){
        this->hide();
    }
    else{
        this->hide();
        TelaGerenciar::frameGerenciarFuncionario->show();
    }

}

void FrameCadastrarFuncionario::recebeDados(Funcionario f){
    limparCamposFuncionario(0);

    Conversoes c;

    //Colando string em QLineEdit
    leNomeFuncionario->setText(QString::fromStdString(f.getNome()));
    leCtpsFuncionario->setText(QString::fromStdString(f.getCTPS()));
    leSenhaFuncionario->setText(QString::fromStdString(f.getSenha()));
    leLoginFuncionario->setText(QString::fromStdString(f.getLogin()));
    leCodigoFuncionario->setText(QString::fromStdString(c.converteIntParaString(f.getCodigo())));

    string dtAdmissao = Data::montaStringData(f.getAdmissao());

    leDataAdmissaoFuncionario->setText(QString::fromStdString(dtAdmissao));
}

Funcionario FrameCadastrarFuncionario::retiraDados(){
    Conversoes c;

    int id = c.converteStringParaInt(leCodigoFuncionario->text().toStdString().c_str());
    string nome = leNomeFuncionario->text().toStdString().c_str();
    string ctps = leCtpsFuncionario->text().toStdString().c_str();
    string login = leLoginFuncionario->text().toStdString().c_str();
    string senha = leSenhaFuncionario->text().toStdString().c_str();
    Data data = Data::montaDataString(leDataAdmissaoFuncionario->text().toStdString().c_str());

    Funcionario f = Funcionario(id,nome,data,ctps,login,senha);

    return f;
}

void FrameCadastrarFuncionario::on_pbCadastrarFuncionario_clicked(){
    if(!(leCodigoFuncionario->text().isEmpty() &&
         leNomeFuncionario->text().isEmpty() &&
         leDataAdmissaoFuncionario->text().isEmpty() &&
         leCtpsFuncionario->text().isEmpty() &&
         leLoginFuncionario->text().isEmpty() &&
         leSenhaFuncionario->text().isEmpty())){

        Funcionario f = retiraDados();

        if(lblTituloTelaFuncionario->text().toStdString().compare("Cadastrar Funcionário") == 0){
            Dados::funcionarioHash.Insere(f,f.getLogin(),Dados::controleCadastroFuncionario);
            TelaGerenciar::frameGerenciarFuncionario->carregarTabelaFuncionario(Dados::funcionarioHash.ListaTodos());
            this->hide();
            Dados::popupMessage->mensagem("Mensagem!","Inserido com sucesso!");
        }
        else if(lblTituloTelaFuncionario->text().toStdString().compare("Editar Funcionário") == 0){
            Dados::funcionarioHash.Edita(f,f.getLogin());
            TelaGerenciar::frameGerenciarFuncionario->carregarTabelaFuncionario(Dados::funcionarioHash.ListaTodos());
            this->hide();
            TelaGerenciar::frameGerenciarFuncionario->show();
            Dados::popupMessage->mensagem("Mensagem!","Editado com sucesso!");
        }

    }
    else{
        Dados::popupMessage->mensagem("Erro!","Preencha todos os campos!");
    }
}
