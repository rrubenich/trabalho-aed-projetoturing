#include "frameefetuarlogin.h"
#include "ui_frameefetuarlogin.h"
#include "Controle/telagerenciar.h"

QLineEdit *leLogin;
QLineEdit *leSenha;

QLabel *lblAlerta;

FrameEfetuarLogin::FrameEfetuarLogin(QWidget *parent) :QFrame(parent),ui(new Ui::FrameEfetuarLogin){
    ui->setupUi(this);

    leLogin = ui->leLogin;
    leSenha = ui->leSenha;

    lblAlerta = ui->lblAlerta;
    lblAlerta->hide();
}

FrameEfetuarLogin::~FrameEfetuarLogin(){
    delete ui;
}

void FrameEfetuarLogin::limparCamposEfetuarLogin(){
    leLogin->clear();
    leSenha->clear();
    lblAlerta->hide();
}

void FrameEfetuarLogin::on_pbLimparCampos_clicked(){
    limparCamposEfetuarLogin();
}

void FrameEfetuarLogin::on_pbEntrarLogin_clicked(){
    string login = leLogin->text().toStdString();
    string senha = leSenha->text().toStdString();

    vector<Funcionario> v = Dados::funcionarioHash.ListaTodos();

    int tam = v.size();
    for(int i = 0; i < tam; i++){
        Funcionario f = v[i];

        if((login.compare(f.getLogin()) == 0) && (senha.compare(f.getSenha()) == 0)){
            lblAlerta->hide();
            Dados::funcionarioLogado = f;

            TelaGerenciar::frameGerenciarEmprestimo->carregarTabelaEmprestimo(Dados::emprestimoHash.ListaTodos());
            TelaGerenciar::frameGerenciarFuncionario->carregarTabelaFuncionario(Dados::funcionarioHash.ListaTodos());
            TelaGerenciar::frameGerenciarLivro->carregarTabelaLivro(Dados::livroHash.ListaTodos());
            TelaGerenciar::frameGerenciarUsuario->carregarTabelaUsuario(Dados::usuarioHash.ListaTodos());

            this->hide();
            break;
        }
        else{
            lblAlerta->show();
        }
    }


}
