#include "framegerenciarfuncionario.h"
#include "ui_framegerenciarfuncionario.h"
#include "Controle/telas.h"

QTableView *tvResultadosFuncionario;
QStandardItemModel *modelFuncionario;
QLineEdit *leCampoBuscaFuncionario;

FrameGerenciarFuncionario::FrameGerenciarFuncionario(QWidget *parent) :QFrame(parent),ui(new Ui::FrameGerenciarFuncionario){
    ui->setupUi(this);
    this->setGeometry(0,20,800,580);

    //Insere colunas tabela
    modelFuncionario = new QStandardItemModel(0,5,this); //0 Rows and 4 Columns
    modelFuncionario->setHorizontalHeaderItem(0, new QStandardItem(QString("ID")));
    modelFuncionario->setHorizontalHeaderItem(1, new QStandardItem(QString("Nome")));
    modelFuncionario->setHorizontalHeaderItem(2, new QStandardItem(QString("Login")));
    modelFuncionario->setHorizontalHeaderItem(3, new QStandardItem(QString("Data Admissão")));
    modelFuncionario->setHorizontalHeaderItem(4, new QStandardItem(QString("CTPS")));

    //Popula tabela
    tvResultadosFuncionario = ui->tvResultados;
    tvResultadosFuncionario->setModel(modelFuncionario);

    //Muda larguras colunas
    tvResultadosFuncionario->setColumnWidth(0, 50);
    tvResultadosFuncionario->setColumnWidth(1, 250);
    tvResultadosFuncionario->setColumnWidth(2, 150);
    tvResultadosFuncionario->setColumnWidth(3, 115);
    tvResultadosFuncionario->setColumnWidth(4, 150);

    leCampoBuscaFuncionario = ui->leCampoBusca;
}

void FrameGerenciarFuncionario::carregarTabelaFuncionario(vector<Funcionario> v){
    Conversoes c;
    int linhas = 0;

    modelFuncionario->removeRows(0,modelFuncionario->rowCount());

    int tam = v.size();
    for(int i = 0; i < tam; i++){
        Funcionario f = v[i];

        QList<QStandardItem*> newRow;

        QStandardItem *Col1 = new QStandardItem(QString(c.converteIntParaString(f.getCodigo()).c_str()));
        newRow.append(Col1);

        QStandardItem *Col2 = new QStandardItem(QString(f.getNome().c_str()));
        newRow.append(Col2);

        QStandardItem *Col3 = new QStandardItem(QString(f.getLogin().c_str()));
        newRow.append(Col3);

        string dtAdmissao = Data::montaStringData(f.getAdmissao());
        QStandardItem *Col4 = new QStandardItem(QString(dtAdmissao.c_str()));
        newRow.append(Col4);

        QStandardItem *Col5 = new QStandardItem(QString(f.getCTPS().c_str()));
        newRow.append(Col5);

        modelFuncionario->insertRow(linhas,newRow);
        linhas++;
    }

}

int FrameGerenciarFuncionario::pegaIDFuncionarioLinhaSelecionada(){
    if(tvResultadosFuncionario->selectionModel()->hasSelection()){
        Conversoes c;

        int row = tvResultadosFuncionario->selectionModel()->currentIndex().row();
        int col = 0;
        int idFuncionario = c.converteStringParaInt(modelFuncionario->item(row,col)->text().toStdString());

        return idFuncionario;
    }
    else{
        return -1;
    }
}

string FrameGerenciarFuncionario::pegaLoginFuncionarioLinhaSelecionada(){
    if(tvResultadosFuncionario->selectionModel()->hasSelection()){
        int row = tvResultadosFuncionario->selectionModel()->currentIndex().row();
        int col = 2;
        string funcionario = modelFuncionario->item(row,col)->text().toStdString();

        return funcionario;
    }
    else{
        return "";
    }
}

void FrameGerenciarFuncionario::limparCamposGerenciarFuncionario(int x){
    if(x == 0){
        modelFuncionario->removeRows(0,modelFuncionario->rowCount());
        leCampoBuscaFuncionario->clear();
    }
    else{
        leCampoBuscaFuncionario->clear();
    }
}

FrameGerenciarFuncionario::~FrameGerenciarFuncionario(){
    delete ui;
}

void FrameGerenciarFuncionario::on_pbCancelar_clicked(){
    this->hide();
}

void FrameGerenciarFuncionario::on_pbEditarFuncionario_clicked(){
    int id = pegaIDFuncionarioLinhaSelecionada();
    string funcionario = pegaLoginFuncionarioLinhaSelecionada();

    if(id != -1){
        Funcionario f = Dados::funcionarioHash.Consulta(funcionario,id);
        Telas::frameCadastrarFuncionario->trocaTituloTelaFuncionario("Editar Funcionário");
        Telas::frameCadastrarFuncionario->recebeDados(f);

        this->hide();
        Telas::frameCadastrarFuncionario->show();
    }
    else{
        Dados::popupMessage->mensagem("Erro!","Tabela não possui linha selecionada!");
    }
}

void FrameGerenciarFuncionario::on_pbMaisInfos_clicked(){
    int id = pegaIDFuncionarioLinhaSelecionada();
    string funcionario = pegaLoginFuncionarioLinhaSelecionada();

    if(id != -1){
        Funcionario f = Dados::funcionarioHash.Consulta(funcionario,id);
        Telas::frameCadastrarFuncionario->trocaTituloTelaFuncionario("Informações Funcionário");
        Telas::frameCadastrarFuncionario->recebeDados(f);

        this->hide();
        Telas::frameCadastrarFuncionario->show();
    }
    else{
        Dados::popupMessage->mensagem("Erro!","Tabela não possui linha selecionada!");
    }
}

void FrameGerenciarFuncionario::on_pbExcluirFuncionario_clicked(){
    int id = pegaIDFuncionarioLinhaSelecionada();
    string funcionario = pegaLoginFuncionarioLinhaSelecionada();

    if(id != -1){
        Funcionario f = Dados::funcionarioHash.Consulta(funcionario,id);
        Dados::funcionarioHash.Deleta(f.getCodigo(),f.getLogin());
        carregarTabelaFuncionario(Dados::funcionarioHash.ListaTodos());
    }
    else{
        Dados::popupMessage->mensagem("Erro!","Tabela não possui linha selecionada!");
    }
}

void FrameGerenciarFuncionario::on_pbBuscar_clicked(){
    string busca = leCampoBuscaFuncionario->text().toStdString();

    if(!busca.empty()){
        Controle aux;
        vector<Funcionario> v = Dados::funcionarioHash.ConsultaUsuarioFuncionarioPorNome(busca,aux);
        carregarTabelaFuncionario(v);
    }else{
        vector<Funcionario> v = Dados::funcionarioHash.ListaTodos();
        carregarTabelaFuncionario(v);
    }

}
