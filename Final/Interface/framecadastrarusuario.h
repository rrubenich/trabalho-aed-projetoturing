#ifndef FRAMECADASTRARUSUARIO_H
#define FRAMECADASTRARUSUARIO_H

#include <QFrame>
#include "Controle/dados.h"
#include "Controle/conversoes.h"

namespace Ui {
class FrameCadastrarUsuario;
}

class FrameCadastrarUsuario : public QFrame
{
    Q_OBJECT

public:
    explicit FrameCadastrarUsuario(QWidget *parent);
    ~FrameCadastrarUsuario();
    void trocaTituloTelaUsuario(string txt);
    void limparCamposUsuario(int x);
    void recebeDados(Usuario u);
    Usuario retiraDados();

private slots:
    void on_pbLimparCampos_clicked();
    void on_pbCancelar_clicked();

    void on_pbCadastrarCliente_clicked();

private:
    Ui::FrameCadastrarUsuario *ui;

};

#endif // FRAMECADASTRARUSUARIO_H
