#ifndef FRAMEEFETUARLOGIN_H
#define FRAMEEFETUARLOGIN_H

#include <QFrame>
#include "Controle/dados.h"

namespace Ui {
class FrameEfetuarLogin;
}

class FrameEfetuarLogin : public QFrame
{
    Q_OBJECT

public:
    explicit FrameEfetuarLogin(QWidget *parent);
    ~FrameEfetuarLogin();
    void limparCamposEfetuarLogin();

private slots:
    void on_pbLimparCampos_clicked();
    void on_pbEntrarLogin_clicked();

private:
    Ui::FrameEfetuarLogin *ui;
};

#endif // FRAMEEFETUARLOGIN_H
