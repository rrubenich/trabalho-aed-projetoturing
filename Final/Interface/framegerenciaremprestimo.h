#ifndef FRAMEGERENCIAREMPRESTIMO_H
#define FRAMEGERENCIAREMPRESTIMO_H

#include "Controle/dados.h"

namespace Ui {
class FrameGerenciarEmprestimo;
}

class FrameGerenciarEmprestimo : public QFrame
{
    Q_OBJECT

public:
    explicit FrameGerenciarEmprestimo(QWidget *parent);
    ~FrameGerenciarEmprestimo();
    void carregarTabelaEmprestimo(vector<Emprestimo> v);
    int pegaIDEmprestimoLinhaSelecionada();
    string pegaLivroEmprestimoLinhaSelecionada();
    QList<QStandardItem*> montaLinhaTabelaEmprestimo(Emprestimo e);
    void limparCamposGerenciarEmprestimo(int x);

private slots:
    void on_pbCancelar_clicked();
    void on_pbEfetuarDevolucao_clicked();
    void on_pbEditarEmprestimo_clicked();

    void on_pbMaisInfos_clicked();

    void on_pbBuscar_clicked();

private:
    Ui::FrameGerenciarEmprestimo *ui;
};

#endif // FRAMEGERENCIAREMPRESTIMO_H
