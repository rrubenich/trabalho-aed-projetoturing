#include "framecadastrarusuario.h"
#include "ui_framecadastrarusuario.h"
#include "Controle/telagerenciar.h"

QLineEdit *leCodigoCliente;
QLineEdit *leRGCliente;
QLineEdit *leCPFCliente;
QLineEdit *leNomeCliente;
QLineEdit *leEmailCliente;
QLineEdit *leTelefoneCliente;
QLineEdit *leEnderecoCliente;

QLabel *lblTituloTelaUsuario;
QPushButton *pbCadastrarUsuario;
QPushButton *pbLimparCamposUsuario;

FrameCadastrarUsuario::FrameCadastrarUsuario(QWidget *parent) :QFrame(parent),ui(new Ui::FrameCadastrarUsuario){
    ui->setupUi(this);
    this->setGeometry(0,20,800,580);

    lblTituloTelaUsuario = ui->lblTitulo;
    pbCadastrarUsuario = ui->pbCadastrarCliente;
    pbLimparCamposUsuario = ui->pbLimparCampos;

    leCodigoCliente = ui->leCodigoCliente;
    leRGCliente = ui->leRGCliente;
    leCPFCliente = ui->leCPFCliente;
    leNomeCliente = ui->leNomeCliente;
    leEmailCliente = ui->leEmailCliente;
    leTelefoneCliente = ui->leTelefoneCliente;
    leEnderecoCliente = ui->leEnderecoCliente;
}

FrameCadastrarUsuario::~FrameCadastrarUsuario(){
    delete ui;
}

void FrameCadastrarUsuario::limparCamposUsuario(int x){
    if(x == 0){
        leCodigoCliente->clear();
        leRGCliente->clear();
        leCPFCliente->clear();
        leNomeCliente->clear();
        leEmailCliente->clear();
        leTelefoneCliente->clear();
        leEnderecoCliente->clear();
    }
    else{
        leRGCliente->clear();
        leCPFCliente->clear();
        leNomeCliente->clear();
        leEmailCliente->clear();
        leTelefoneCliente->clear();
        leEnderecoCliente->clear();
    }
}

void FrameCadastrarUsuario::trocaTituloTelaUsuario(string txt){
    lblTituloTelaUsuario->setText(QString::fromStdString(txt));
    pbCadastrarUsuario->setText(QString::fromStdString(txt));

    if(txt.compare("Informações Cliente") == 0){
        pbCadastrarUsuario->hide();
        pbLimparCamposUsuario->hide();
        leRGCliente->setEnabled(false);
        leCPFCliente->setEnabled(false);
        leNomeCliente->setEnabled(false);
        leEmailCliente->setEnabled(false);
        leTelefoneCliente->setEnabled(false);
        leEnderecoCliente->setEnabled(false);
    }
    else{
        pbCadastrarUsuario->show();
        pbLimparCamposUsuario->show();
        leRGCliente->setEnabled(true);
        leCPFCliente->setEnabled(true);
        leNomeCliente->setEnabled(true);
        leEmailCliente->setEnabled(true);
        leTelefoneCliente->setEnabled(true);
        leEnderecoCliente->setEnabled(true);
    }
}

void FrameCadastrarUsuario::on_pbLimparCampos_clicked(){
    limparCamposUsuario(1);
}

void FrameCadastrarUsuario::on_pbCancelar_clicked(){
    limparCamposUsuario(0);
    if(lblTituloTelaUsuario->text().toStdString().compare("Cadastrar Cliente") == 0){
        this->hide();
    }
    else{
        this->hide();
        TelaGerenciar::frameGerenciarUsuario->show();
    }
}

void FrameCadastrarUsuario::recebeDados(Usuario u){
    limparCamposUsuario(0);
    Conversoes c;

    leCodigoCliente->setText(c.converteIntParaString(u.getCodigo()).c_str());
    leRGCliente->setText(u.getRG().c_str());
    leCPFCliente->setText(u.getCPF().c_str());
    leNomeCliente->setText(u.getNome().c_str());
    leEmailCliente->setText(u.getEmail().c_str());
    leTelefoneCliente->setText(u.getTelefone().c_str());
    leEnderecoCliente->setText(u.getEndereco().getEndereco().c_str());

}

Usuario FrameCadastrarUsuario::retiraDados(){
    Conversoes c;

    int id = c.converteStringParaInt(leCodigoCliente->text().toStdString());
    string rg = leRGCliente->text().toStdString();
    string cpf = leCPFCliente->text().toStdString();
    string nome = leNomeCliente->text().toStdString();
    string email = leEmailCliente->text().toStdString();
    string telefone = leTelefoneCliente->text().toStdString();
    Endereco endereco = Endereco(leEnderecoCliente->text().toStdString());

    Usuario u = Usuario(id,nome,rg,cpf,email,telefone,endereco);

    return u;
}

void FrameCadastrarUsuario::on_pbCadastrarCliente_clicked(){
    if(!(leCodigoCliente->text().isEmpty() &&
         leRGCliente->text().isEmpty() &&
         leCPFCliente->text().isEmpty() &&
         leNomeCliente->text().isEmpty() &&
         leEmailCliente->text().isEmpty() &&
         leTelefoneCliente->text().isEmpty() &&
         leEnderecoCliente->text().isEmpty())){

        Usuario u = retiraDados();

        if(lblTituloTelaUsuario->text().toStdString().compare("Cadastrar Cliente") == 0){
            Dados::usuarioHash.Insere(u,u.getNome(),Dados::controleCadastroUsuario);
            TelaGerenciar::frameGerenciarUsuario->carregarTabelaUsuario(Dados::usuarioHash.ListaTodos());
            this->hide();
            Dados::popupMessage->mensagem("Mensagem!","Inserido com sucesso!");
        }
        else if(lblTituloTelaUsuario->text().toStdString().compare("Editar Cliente") == 0){
            Dados::usuarioHash.Edita(u,u.getNome());
            TelaGerenciar::frameGerenciarUsuario->carregarTabelaUsuario(Dados::usuarioHash.ListaTodos());
            this->hide();
            TelaGerenciar::frameGerenciarUsuario->show();
            Dados::popupMessage->mensagem("Mensagem!","Editado com sucesso!");
        }

    }
    else{
        Dados::popupMessage->mensagem("Erro!","Preencha todos os campos!");
    }
}
