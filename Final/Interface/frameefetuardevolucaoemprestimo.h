#ifndef FRAMEEFETUARDEVOLUCAOEMPRESTIMO_H
#define FRAMEEFETUARDEVOLUCAOEMPRESTIMO_H

#include <QFrame>
#include "Controle/dados.h"
#include "Controle/conversoes.h"

namespace Ui {
class FrameEfetuarDevolucaoEmprestimo;
}

class FrameEfetuarDevolucaoEmprestimo : public QFrame
{
    Q_OBJECT

public:
    explicit FrameEfetuarDevolucaoEmprestimo(QWidget *parent);
    ~FrameEfetuarDevolucaoEmprestimo();
    void recebeDados(Emprestimo e);
    void limparCamposEfetuarDevolucaoEmprestimo();
    int calculaDiasAposDevolucao(Data devolucao, Data atual);
    int bissexto (int ano);

private slots:
    void on_pbCancelar_clicked();

    void on_pbEfetuarDevolucao_clicked();

private:
    Ui::FrameEfetuarDevolucaoEmprestimo *ui;

};

#endif // FRAMEEFETUARDEVOLUCAOEMPRESTIMO_H
