#ifndef FRAMEGERENCIARFUNCIONARIO_H
#define FRAMEGERENCIARFUNCIONARIO_H

#include "Controle/dados.h"

namespace Ui {
class FrameGerenciarFuncionario;
}

class FrameGerenciarFuncionario : public QFrame
{
    Q_OBJECT

public:
    explicit FrameGerenciarFuncionario(QWidget *parent);
    ~FrameGerenciarFuncionario();
    void carregarTabelaFuncionario(vector<Funcionario> v);
    int pegaIDFuncionarioLinhaSelecionada();
    string pegaLoginFuncionarioLinhaSelecionada();
    void limparCamposGerenciarFuncionario(int x);

private slots:
    void on_pbCancelar_clicked();

    void on_pbEditarFuncionario_clicked();

    void on_pbExcluirFuncionario_clicked();

    void on_pbMaisInfos_clicked();

    void on_pbBuscar_clicked();

private:
    Ui::FrameGerenciarFuncionario *ui;
};

#endif // FRAMEGERENCIARFUNCIONARIO_H
