#ifndef FRAMECADASTRARFUNCIONARIO_H
#define FRAMECADASTRARFUNCIONARIO_H

#include <QFrame>
#include "Controle/dados.h"
#include "Controle/conversoes.h"

namespace Ui {
class FrameCadastrarFuncionario;
}

class FrameCadastrarFuncionario : public QFrame
{
    Q_OBJECT

public:
    explicit FrameCadastrarFuncionario(QWidget *parent);
    ~FrameCadastrarFuncionario();
    void trocaTituloTelaFuncionario(string txt);
    void recebeDados(Funcionario f);
    Funcionario retiraDados();
    void limparCamposFuncionario(int x);

private slots:
    void on_pbLimparCampos_clicked();
    void on_pbCancelar_clicked();

    void on_pbCadastrarFuncionario_clicked();

private:
    Ui::FrameCadastrarFuncionario *ui;

};

#endif // FRAMECADASTRARFUNCIONARIO_H
