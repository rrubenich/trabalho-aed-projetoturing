#ifndef FRAMEGERENCIARLIVRO_H
#define FRAMEGERENCIARLIVRO_H

#include "Controle/dados.h"

namespace Ui {
class FrameGerenciarLivro;
}

class FrameGerenciarLivro : public QFrame{
    Q_OBJECT

public:
    explicit FrameGerenciarLivro(QWidget *parent);
    ~FrameGerenciarLivro();
    void carregarTabelaLivro(vector<Livro> v);
    QList<QStandardItem*> montaLinhaTabelaLivro(Livro l);
    int pegaIDLivroLinhaSelecionada();
    string pegaNomeLivroLinhaSelecionada();
    void limparCamposGerenciarLivro(int x);
    void populaComboBoxAreas();

private slots:
    void on_pbCancelar_clicked();

    void on_pbEditarLivro_clicked();

    void on_pbExcluirLivro_clicked();

    void on_pbMaisInfos_clicked();

    void on_pbBuscar_clicked();

    void on_cbFiltroBusca_activated(const QString &arg1);

    void on_cbFiltroBusca_activated(int index);

    void on_cbFiltroLivroAreas_activated(const QString &arg1);

private:
    Ui::FrameGerenciarLivro *ui;
};

#endif // FRAMEGERENCIARLIVRO_H
