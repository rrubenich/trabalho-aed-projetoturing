#include "framegerenciaremprestimo.h"
#include "ui_framegerenciaremprestimo.h"
#include "Controle/telas.h"

QTableView *tvResultadosEmprestimo;
QStandardItemModel *modelEmprestimo;
QComboBox *cbFiltroEmprestimo;
QLineEdit *leCampoBuscaEmprestimo;

FrameGerenciarEmprestimo::FrameGerenciarEmprestimo(QWidget *parent) :QFrame(parent), ui(new Ui::FrameGerenciarEmprestimo){
    ui->setupUi(this);
    this->setGeometry(0,20,800,580);

    //Insere colunas tabela
    modelEmprestimo = new QStandardItemModel(0,6,this); //0 Rows and 5 Columns
    modelEmprestimo->setHorizontalHeaderItem(0, new QStandardItem(QString("ID")));
    modelEmprestimo->setHorizontalHeaderItem(1, new QStandardItem(QString("Livro")));
    modelEmprestimo->setHorizontalHeaderItem(2, new QStandardItem(QString("Data Devolução")));
    modelEmprestimo->setHorizontalHeaderItem(3, new QStandardItem(QString("Data Empréstimo")));
    modelEmprestimo->setHorizontalHeaderItem(4, new QStandardItem(QString("Cliente")));
    modelEmprestimo->setHorizontalHeaderItem(5, new QStandardItem(QString("Funcionário")));

    tvResultadosEmprestimo = ui->tvResultados;
    tvResultadosEmprestimo->setModel(modelEmprestimo);

    //Muda larguras colunas
    tvResultadosEmprestimo->setColumnWidth(0, 50);
    tvResultadosEmprestimo->setColumnWidth(1, 200);
    tvResultadosEmprestimo->setColumnWidth(2, 100);
    tvResultadosEmprestimo->setColumnWidth(3, 100);
    tvResultadosEmprestimo->setColumnWidth(4, 125);
    tvResultadosEmprestimo->setColumnWidth(5, 125);

    //Popula combobox
    cbFiltroEmprestimo = ui->cbFiltroBusca;
    cbFiltroEmprestimo->addItem("Data");
    cbFiltroEmprestimo->addItem("Funcionário");
    cbFiltroEmprestimo->addItem("Cliente");

    leCampoBuscaEmprestimo = ui->leCampoBusca;
}

QList<QStandardItem*> FrameGerenciarEmprestimo::montaLinhaTabelaEmprestimo(Emprestimo e){
    Conversoes c;
    QList<QStandardItem*> newRow;

    QStandardItem *Col1 = new QStandardItem(QString(c.converteIntParaString(e.getCodigo()).c_str()));
    newRow.append(Col1);

    QStandardItem *Col2 = new QStandardItem(QString(e.getLivro().getTitulo().c_str()));
    newRow.append(Col2);

    string dtDevolucao = Data::montaStringDataDevolucao(e.getData(),5);
    Data::montaDataString(dtDevolucao);

    QStandardItem *Col3 = new QStandardItem(QString(dtDevolucao.c_str()));
    newRow.append(Col3);

    string dtEmprestimo = Data::montaStringData(e.getData());
    Data::montaDataString(dtEmprestimo);

    QStandardItem *Col4 = new QStandardItem(QString(dtEmprestimo.c_str()));
    newRow.append(Col4);

    QStandardItem *Col5 = new QStandardItem(QString(e.getUsuario().getNome().c_str()));
    newRow.append(Col5);

    QStandardItem *Col6 = new QStandardItem(QString(e.getFuncionario().getNome().c_str()));
    newRow.append(Col6);

    return newRow;
}

void FrameGerenciarEmprestimo::carregarTabelaEmprestimo(vector<Emprestimo> v){
    int linhas = 0;

    modelEmprestimo->removeRows(0,modelEmprestimo->rowCount());

    int tam = v.size();
    for(int i = 0; i < tam; i++){
        Emprestimo e = v[i];
        modelEmprestimo->insertRow(linhas,montaLinhaTabelaEmprestimo(e));
        linhas++;
    }

}

int FrameGerenciarEmprestimo::pegaIDEmprestimoLinhaSelecionada(){
    if(tvResultadosEmprestimo->selectionModel()->hasSelection()){
        Conversoes c;

        int row = tvResultadosEmprestimo->selectionModel()->currentIndex().row();
        int col = 0;
        int idEmprestimos = c.converteStringParaInt(modelEmprestimo->item(row,col)->text().toStdString());

        return idEmprestimos;
    }
    else{
        return -1;
    }
}

string FrameGerenciarEmprestimo::pegaLivroEmprestimoLinhaSelecionada(){
    if(tvResultadosEmprestimo->selectionModel()->hasSelection()){
        Conversoes c;

        int row = tvResultadosEmprestimo->selectionModel()->currentIndex().row();
        int col = 1;
        string emprestimos = modelEmprestimo->item(row,col)->text().toStdString();

        return emprestimos;
    }
    else{
        return "";
    }
}

void FrameGerenciarEmprestimo::limparCamposGerenciarEmprestimo(int x){
    if(x == 0){
        modelEmprestimo->removeRows(0,modelEmprestimo->rowCount());
        cbFiltroEmprestimo->setCurrentIndex(0);
        leCampoBuscaEmprestimo->clear();
    }
    else{
        cbFiltroEmprestimo->setCurrentIndex(0);
        leCampoBuscaEmprestimo->clear();
    }
}

FrameGerenciarEmprestimo::~FrameGerenciarEmprestimo(){
    delete ui;
}

void FrameGerenciarEmprestimo::on_pbCancelar_clicked(){
    this->hide();
}

void FrameGerenciarEmprestimo::on_pbEfetuarDevolucao_clicked(){

    int id = pegaIDEmprestimoLinhaSelecionada();
    string livro = pegaLivroEmprestimoLinhaSelecionada();

    if(id != -1){
        Emprestimo e = Dados::emprestimoHash.ConsultaEmprestimo(livro);

        this->hide();
        Telas::frameEfetuarDevolucaoEmprestimo->recebeDados(e);
        Telas::frameEfetuarDevolucaoEmprestimo->show();
    }
    else{
        Dados::popupMessage->mensagem("Erro!","Tabela não possui linha selecionada!");
    }
}

void FrameGerenciarEmprestimo::on_pbEditarEmprestimo_clicked(){
    int id = pegaIDEmprestimoLinhaSelecionada();
    string livro = pegaLivroEmprestimoLinhaSelecionada();

    if(id != -1){
        Emprestimo e = Dados::emprestimoHash.ConsultaEmprestimo(livro);
        Telas::frameCadastrarEmprestimo->trocaTituloTelaEmprestimo("Editar Empréstimo");
        Telas::frameCadastrarEmprestimo->recebeDados(e);

        this->hide();
        Telas::frameCadastrarEmprestimo->show();
    }
    else{
        Dados::popupMessage->mensagem("Erro!","Tabela não possui linha selecionada!");
    }
}

void FrameGerenciarEmprestimo::on_pbMaisInfos_clicked(){
    int id = pegaIDEmprestimoLinhaSelecionada();
    string livro = pegaLivroEmprestimoLinhaSelecionada();

    if(id != -1){
        Emprestimo e = Dados::emprestimoHash.ConsultaEmprestimo(livro);
        Telas::frameCadastrarEmprestimo->trocaTituloTelaEmprestimo("Informações Empréstimo");
        Telas::frameCadastrarEmprestimo->recebeDados(e);

        this->hide();
        Telas::frameCadastrarEmprestimo->show();
    }
    else{
        Dados::popupMessage->mensagem("Erro!","Tabela não possui linha selecionada!");
    }
}

void FrameGerenciarEmprestimo::on_pbBuscar_clicked(){
    string busca = leCampoBuscaEmprestimo->text().toStdString();

    if ((cbFiltroEmprestimo->currentIndex() == 0) ) {
        //data
        if(!busca.empty()){
            vector<Emprestimo> v = Dados::emprestimoHash.ConsultaEmprestimosPorData(busca,Dados::controleConsultaEmprestimoData);
            carregarTabelaEmprestimo(v);
        }else{
            vector<Emprestimo> v = Dados::emprestimoHash.ListaTodos();
            carregarTabelaEmprestimo(v);
        }
    }
    if ((cbFiltroEmprestimo->currentIndex() == 1) ) {
        //funcionario
        if(!busca.empty()){
            vector<Emprestimo> v = Dados::emprestimoHash.ConsultaEmprestimosPorFuncionario(busca,Dados::controleConsultaEmprestimoFuncionario);
            carregarTabelaEmprestimo(v);
        }else{
            vector<Emprestimo> v = Dados::emprestimoHash.ListaTodos();
            carregarTabelaEmprestimo(v);
        }
    }
    if ((cbFiltroEmprestimo->currentIndex() == 2) ) {
        //usuario
        if(!busca.empty()){
            vector<Emprestimo> v = Dados::emprestimoHash.ConsultaEmprestimosPorUsuario(busca,Dados::controleConsultaEmprestimoUsuario);
            carregarTabelaEmprestimo(v);
        }else{
            vector<Emprestimo> v = Dados::emprestimoHash.ListaTodos();
            carregarTabelaEmprestimo(v);
        }
    }

}
