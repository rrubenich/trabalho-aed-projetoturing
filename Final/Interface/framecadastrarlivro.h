#ifndef FRAMECADASTRARLIVRO_H
#define FRAMECADASTRARLIVRO_H

#include <QFrame>
#include "Controle/dados.h"
#include "Controle/conversoes.h"

namespace Ui {
class FrameCadastrarLivro;
}

class FrameCadastrarLivro : public QFrame
{
    Q_OBJECT

public:
    explicit FrameCadastrarLivro(QWidget *parent);
    ~FrameCadastrarLivro();
    void trocaTituloTelaLivro(string txt);
    void limparCamposLivro(int x);
    void populaComboBoxAreas();
    void recebeDados(Livro l);
    Livro retiraDados();

private slots:
    void on_pbLimparCampos_clicked();
    void on_pbCancelar_clicked();

    void on_pbCadastrarLivro_clicked();

private:
    Ui::FrameCadastrarLivro *ui;

};

#endif // FRAMECADASTRARLIVRO_H
