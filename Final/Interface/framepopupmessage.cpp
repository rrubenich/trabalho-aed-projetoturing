#include "framepopupmessage.h"
#include "ui_framepopupmessage.h"

FramePopupMessage::FramePopupMessage(QWidget *parent) :QFrame(parent),ui(new Ui::FramePopupMessage){
    ui->setupUi(this);
    this->hide();
}

void FramePopupMessage::mensagem(std::string title, std::string msg){
    ui->lblTitulo->setText(title.c_str());
    ui->label->setText(msg.c_str());
    this->show();
}

FramePopupMessage::~FramePopupMessage(){
    delete ui;
}

void FramePopupMessage::on_pushButton_clicked(){
    this->hide();
}
