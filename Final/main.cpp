using namespace std;

#include <QApplication>
#include "Controle/dados.h"
#include "Controle/telas.h"
#include "Controle/telagerenciar.h"
#include "Interface/mainwindow.h"

HashAberto<Livro> Dados::livroHash;
HashAberto<Usuario> Dados::usuarioHash;
HashAberto<Emprestimo> Dados::emprestimoHash;
HashAberto<Funcionario> Dados::funcionarioHash;
HashAberto<Area> Dados::areaHash;
ArquivoLog Dados::listaLogs;
Funcionario Dados::funcionarioLogado;

FrameCadastrarLivro *Telas::frameCadastrarLivro;
FrameCadastrarUsuario *Telas::frameCadastrarUsuario;
FrameCadastrarFuncionario *Telas::frameCadastrarFuncionario;
FrameCadastrarEmprestimo *Telas::frameCadastrarEmprestimo;
FrameEfetuarDevolucaoEmprestimo *Telas::frameEfetuarDevolucaoEmprestimo;

FrameGerenciarFuncionario *TelaGerenciar::frameGerenciarFuncionario;
FrameGerenciarLivro *TelaGerenciar::frameGerenciarLivro;
FrameGerenciarUsuario *TelaGerenciar::frameGerenciarUsuario;
FrameGerenciarEmprestimo *TelaGerenciar::frameGerenciarEmprestimo;
FrameEfetuarLogin *TelaGerenciar::frameEfetuarLogin;

FramePopupMessage *Dados::popupMessage;

int main(int argc, char *argv[]){
    QApplication a(argc, argv);

    MainWindow w;
    w.show();

    Dados d;
    d.populaHash();

    return a.exec();
}
