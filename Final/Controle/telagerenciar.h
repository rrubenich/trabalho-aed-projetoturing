#ifndef TELAGERENCIAR_H
#define TELAGERENCIAR_H

#include "Interface/frameefetuarlogin.h"
#include "Interface/framegerenciaremprestimo.h"
#include "Interface/framegerenciarfuncionario.h"
#include "Interface/framegerenciarlivro.h"
#include "Interface/framegerenciarusuario.h"

class TelaGerenciar{
public:
    TelaGerenciar();

    static FrameGerenciarFuncionario *frameGerenciarFuncionario;
    static FrameGerenciarLivro *frameGerenciarLivro;
    static FrameGerenciarUsuario *frameGerenciarUsuario;
    static FrameGerenciarEmprestimo *frameGerenciarEmprestimo;
    static FrameEfetuarLogin *frameEfetuarLogin;
};

#endif // TELAGERENCIAR_H
