#ifndef CONVERSOES_H
#define CONVERSOES_H

#include <string>

using namespace std;

class Conversoes{
public:
    Conversoes();
    int converteStringParaInt(string s);
    string converteIntParaString(int i);
};

#endif // CONVERSOES_H
