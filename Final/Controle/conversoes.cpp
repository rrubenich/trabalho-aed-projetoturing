#include "Controle/dados.h"
#include "Controle/conversoes.h"

using namespace std;

Conversoes::Conversoes(){

}

int Conversoes::converteStringParaInt(string s){
    return atoi(s.c_str());
}

string Conversoes::converteIntParaString(int i){
    ostringstream convert;
    convert.str("");
    convert << i;
    return convert.str();
}
